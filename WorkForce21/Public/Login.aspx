﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="WorkForce21.Public.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>WorkForce21</title>
    <link rel="stylesheet" href="<%= Page.ResolveClientUrl("~/Content/bootstrap.min.css") %>" type="text/css" />
    <link rel="stylesheet" href="<%= Page.ResolveClientUrl("~/Content/styles.css") %>" type="text/css" />

    <script type="text/javascript" src="<%= Page.ResolveClientUrl("~/Scripts/jquery-2.1.3.min.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveClientUrl("~/Scripts/jquery-ui-1.11.2.min.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveClientUrl("~/Scripts/modernizr-2.6.2.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveClientUrl("~/Scripts/bootstrap.min.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveClientUrl("~/Scripts/scripts.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveClientUrl("~/Scripts/jquery.bootstrap.wizard.min.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveClientUrl("~/Scripts/jquery.jgrowl.min.js") %>"></script>
    <script src="<%= Page.ResolveClientUrl("~/scripts/custom/globals.js") %>"></script>

    <script type="text/javascript">
        var baseUrl = '/workforce21';
        jQuery(document).ready(function () {
            jQuery('#aForgotPassword').unbind('click').click(function () {
                jQuery('#resetPassword').modal('show');
                return false;
            });

            jQuery('#aNewUser').unbind('click').click(function () {
                jQuery('#registerUser').modal('show');
                return false;
            });

            jQuery('#cmdResetPassword').unbind('click').click(function () {
                var username = jQuery('#txtUsername').val();
                resetPassword(function (result) {
                    if (result == 'Success') {
                        showMessage('Password successfully changed! Please check your email.');
                        jQuery('#resetPassword').modal('hide');
                    }
                    else showMessage(result);
                }, username);
            });

            jQuery('#cmdRegisterUser').unbind('click').click(function () {
                var username = jQuery('#txtRegisterEmail').val();
                registerUser(function (result) {
                    if (result == 'Success') {
                        showMessage('Registration successful! Please check your email.');
                        jQuery('#registerUser').modal('hide');
                    }
                    else showMessage(result);
                }, username);
            });
        });

        function resetPassword(callback, username) {
            $.ajax({
                url: baseUrl + '/administration/clients/resetpassword',
                data: '=' + username,
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    callback.call(this, data);
                },
                error: function (error) {
                    showMessage(error.responseText);
                }
            });
        }

        function registerUser(callback, username) {
            $.ajax({
                url: baseUrl + '/administration/clients/registeruser',
                data: '=' + username,
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    callback.call(this, data);
                },
                error: function (error) {
                    showMessage(error.responseText);
                }
            });
        }
    </script>
</head>
<body>
    <!--login modal-->
    <div id="loginModal" class="modal show" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>--%>
                    <h1 class="text-center">Login</h1>
                </div>
                <div class="modal-body">
                    <form runat="server" class="form col-md-12 center-block">
                        <asp:Login ID="liLogin" runat="server" OnAuthenticate="OnAuthenticate" RenderOuterTable="false">
                            <LayoutTemplate>
                                <div class="form-group">
                                    <asp:TextBox runat="server" ID="UserName" CssClass="form-control input-lg" placeholder="Email" />
                                </div>
                                <div class="form-group">
                                    <asp:TextBox TextMode="password" runat="server" ID="Password" CssClass="form-control input-lg" placeholder="Password" />
                                </div>
                                <div class="form-group">
                                    <asp:Panel runat="server" ID="FailureText" Visible="false">
                                        <div class="alert alert-danger" role="alert">
                                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                            <span class="sr-only">Error:</span>
                                            Enter a valid email address or password
                                        </div>
                                    </asp:Panel>
                                </div>
                                <div class="form-group">
                                    <asp:Button runat="server" ID="LoginButton" CssClass="btn btn-primary btn-lg btn-block" CommandName="Login" ValidationGroup="test" Text="Sign In" name="submit" />
                                    <span><a href="#" id="aForgotPassword">Forgot Password?</a></span>
                                    <span class="pull-right"><a href="#" id="aNewUser">New User?</a></span>
                                </div>
                            </LayoutTemplate>
                        </asp:Login>
                    </form>
                </div>
                <div class="modal-footer">
                    <div class="col-md-12">
                        <%--<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>--%>
                        <p>&copy; <%=DateTime.Now.Year %> WorkForce21. All Rights Reserved.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--loginpanel-->

    <div id="resetPassword" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Recover Password</h4>
                </div>
                <div class="modal-body">
                    <p>
                        <input type="text" id="txtUsername" class="form-control account-inputs" placeholder="Email..." />
                    </p>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                    <button class="btn btn-primary" id="cmdResetPassword">Recover Password</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div id="registerUser" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Register with WorkForce21</h4>
                </div>
                <div class="modal-body">
                    <p>
                        <input type="text" id="txtRegisterEmail" class="form-control account-inputs" placeholder="Email..." />
                    </p>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                    <button class="btn btn-primary" id="cmdRegisterUser">Register</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
</body>
</html>
