﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using WorkForce21.DAL;
using WorkForce21.Models;

namespace WorkForce21.Public
{
    public partial class Login : System.Web.UI.Page
    {
        private WFContext db = new WFContext();

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.ClientScript.RegisterArrayDeclaration("alertmethod", JsonConvert.SerializeObject("Growl"));

            string action = Request["action"];
            if (!string.IsNullOrEmpty(action) && action == "logout")
            {
                Session.Clear();
                FormsAuthentication.SignOut();
            }
            else if (!Request.RawUrl.Contains("Questionnaire")) Response.Redirect("Login?ReturnUrl=Questionnaire");
        }

        protected void OnAuthenticate(object sender, AuthenticateEventArgs e)
        {
            string username = liLogin.UserName.Trim();
            string password = liLogin.Password.Trim();

            Client client = db.Clients.FirstOrDefault(a => a.Email == username /* && a.Password == password TO DO */);
            if (client != null && client.Active)
            {
                Session["Client"] = client;
                e.Authenticated = true;
            }
            else liLogin.FindControl("FailureText").Visible = true;
        }
    }
}