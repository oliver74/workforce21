﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using WorkForce21.DAL;
using WorkForce21.Models;
using WorkForce21.Utilities;

namespace WorkForce21
{
    public partial class SiteMaster : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.ClientScript.RegisterArrayDeclaration("alertmethod", JsonConvert.SerializeObject("Growl"));
            Client client = (Client)Session["Client"];
            if (client == null) Response.Redirect("~/login?action=logout");
            else if (!client.Admin)
            {
                menuAdministration.Visible = false;

                // Check what forms the client has paid for
                List<string> purchasedForms = Helpers.GetPurchasedForms(client.ID);

                if (!purchasedForms.Contains("I9")) menuI9.Visible = false;
                if (!purchasedForms.Contains("ADA")) menuADA.Visible = false;
                if (!purchasedForms.Contains("FLSA")) menuFLSA.Visible = false;
                if (!purchasedForms.Contains("ADEA")) menuADEA.Visible = false;
                if (!purchasedForms.Contains("FMLA")) menuFMLA.Visible = false;
                if (!purchasedForms.Contains("HPS")) menuHPS.Visible = false;
                if (!purchasedForms.Contains("CR")) menuContractor.Visible = false;
            }
        }
    }
}