﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using WorkForce21.Migrations;
using WorkForce21.Models;

namespace WorkForce21.DAL
{
    public class WFContext : DbContext
    {
        public WFContext()
            : base("WFConnectionString")
        {
            Database.SetInitializer<WFContext>(new MigrateDatabaseToLatestVersion<WFContext, Configuration>());
            Database.Initialize(false);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Entity<I9Item>().HasMany(e => e.CustomAnswers).WithMany()
               .Map(x =>
               {
                   x.MapLeftKey("I9ItemId");
                   x.MapRightKey("CustomAnswerId");
                   x.ToTable("I9ItemCustomAnswersMapping");
               });
            modelBuilder.Entity<AdaItem>().HasMany(e => e.CustomAnswers).WithMany()
               .Map(x =>
               {
                   x.MapLeftKey("AdaItemId");
                   x.MapRightKey("CustomAnswerId");
                   x.ToTable("AdaItemCustomAnswersMapping");
               });
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Client> Clients { get; set; }
        public DbSet<Questionnaire> Questionnaires { get; set; }
        public DbSet<CustomAnswer> CustomAnswers { get; set; }
        public DbSet<I9Item> I9Items { get; set; }
        public DbSet<AdaItem> AdaItems { get; set; }
        public DbSet<FlsaItem> FlsaItems { get; set; }
        public DbSet<FmlaItem> FmlaItems { get; set; }
        public DbSet<HPSponsorItem> HPSponsorItems { get; set; }
        public DbSet<ContractorItem> ContractorItems { get; set; }
        public DbSet<ButtonOption> ButtonOptions { get; set; }
        public DbSet<I9Answer> I9Answers { get; set; }
        public DbSet<AdaAnswer> AdaAnswers { get; set; }
        public DbSet<FlsaAnswer> FlsaAnswers { get; set; }
        public DbSet<FmlaAnswer> FmlaAnswers { get; set; }
        public DbSet<HPSponsorAnswer> HPSponsorAnswers { get; set; }
        public DbSet<ContractorAnswer> ContractorAnswers { get; set; }
        public DbSet<Purchase> Purchases { get; set; }
    }
}