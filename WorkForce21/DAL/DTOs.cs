﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WorkForce21.DAL
{
    public class ChangePwdDto
    {
        public int ID { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }

    public class SubmitAnswerDto
    {
        public int ItemID { get; set; }
        public int AnswerID { get; set; }
    }
}