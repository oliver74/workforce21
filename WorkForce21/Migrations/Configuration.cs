namespace WorkForce21.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
using WorkForce21.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<WorkForce21.DAL.WFContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = false;
            ContextKey = "WorkForce21.DAL.WFContext";
        }

        protected override void Seed(WorkForce21.DAL.WFContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            I9ItemsAddOrUpdate(context);
            AdaItemsAddOrUpdate(context);
            FlsaItemsAddOrUpdate(context);
            FmlaItemsAddOrUpdate(context);
            HPSponsorItemsAddOrUpdate(context);
            ContractorItemsAddOrUpdate(context);
            CustomAnswersAddOrUpdate(context);
            ButtonOptionsAddOrUpdate(context);
        }

        private void I9ItemsAddOrUpdate(WorkForce21.DAL.WFContext context)
        {
            // Panel One
            context.I9Items.AddOrUpdate(i9 => i9.ID, new I9Item { ID = 1, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 1, AnswerType = "Radio", Question = "Our I-9 forms are stored in what format:" });
            context.I9Items.AddOrUpdate(i9 => i9.ID, new I9Item { ID = 2, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 2, AnswerType = "Button", Question = "The I-9 verification processes only commences after an applicant has accepted an offer of employment." });
            context.I9Items.AddOrUpdate(i9 => i9.ID, new I9Item { ID = 3, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 3, AnswerType = "Button", Question = "A form I-9 is completed for every new employee within 3 days of hire (includes the first day of hire)." });
            context.I9Items.AddOrUpdate(i9 => i9.ID, new I9Item { ID = 4, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 4, AnswerType = "Button", Question = "As the official document, we only store the English language version of Form I-9." });
            context.I9Items.AddOrUpdate(i9 => i9.ID, new I9Item { ID = 5, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 5, AnswerType = "Button", Question = "The person(s) completing the Form I-9 have been trained on, or are properly experienced in the rules for properly completing a Form I-9." });
            context.I9Items.AddOrUpdate(i9 => i9.ID, new I9Item { ID = 6, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 6, AnswerType = "Button", Question = "The I-9 process is applied consistently for all employees." });
            context.I9Items.AddOrUpdate(i9 => i9.ID, new I9Item { ID = 7, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 7, AnswerType = "Button", Question = "We conduct an annual I-9 desk audit annually to ensure compliance with the I-9 requirements." });
            context.I9Items.AddOrUpdate(i9 => i9.ID, new I9Item { ID = 8, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 8, AnswerType = "Button", Question = "We ensure that the most current approved Form I-9 is used by checking the effective date" });
            context.I9Items.AddOrUpdate(i9 => i9.ID, new I9Item { ID = 9, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 9, AnswerType = "Button", Question = "The completed Form I-9 is maintained in a separate file, away from the employees personnel file." });
            context.I9Items.AddOrUpdate(i9 => i9.ID, new I9Item { ID = 10, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 10, AnswerType = "Button", Question = "When employees terminate employment, their Form I-9 is moved to a �Terminated Employees� file and a destruction date is identified when the document may be destroyed." });
            context.I9Items.AddOrUpdate(i9 => i9.ID, new I9Item { ID = 11, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 11, AnswerType = "Button", Question = "I-9�s are always shredded prior to disposal." });
            context.I9Items.AddOrUpdate(i9 => i9.ID, new I9Item { ID = 12, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 12, AnswerType = "Button", Question = "I-9 documents on terminated employees are destroyed within two weeks after their destruction date is reached." });
            context.I9Items.AddOrUpdate(i9 => i9.ID, new I9Item { ID = 13, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 13, AnswerType = "Button", Question = "I-9 documents can be retrieved within three (3) days of an inspection request by government officials." });

            // Panel Two
            context.I9Items.AddOrUpdate(i9 => i9.ID, new I9Item { ID = 14, HtmlContainer = "collapsePanelTwoInner", OrderInContainer = 1, AnswerType = "Button", Question = "The employer does not require or request any particular document for verification other than those allowed by List A, List B, or List C of the I-9" });
            context.I9Items.AddOrUpdate(i9 => i9.ID, new I9Item { ID = 15, HtmlContainer = "collapsePanelTwoInner", OrderInContainer = 2, AnswerType = "Button", Question = "The employer does not accept copies or faxed versions of documents.  All document must be an original." });
            context.I9Items.AddOrUpdate(i9 => i9.ID, new I9Item { ID = 16, HtmlContainer = "collapsePanelTwoInner", OrderInContainer = 3, AnswerType = "Button", Question = "The employer does not accept documents that are expired." });
            context.I9Items.AddOrUpdate(i9 => i9.ID, new I9Item { ID = 17, HtmlContainer = "collapsePanelTwoInner", OrderInContainer = 4, AnswerType = "Button", Question = "The employer does not seek any additional documentation or information beyond what is required by Form I-9." });
            context.I9Items.AddOrUpdate(i9 => i9.ID, new I9Item { ID = 18, HtmlContainer = "collapsePanelTwoInner", OrderInContainer = 5, AnswerType = "Button", Question = "The employer or employer�s representative reviews the documents presented in person to confirm that they belong to the presenting employee and to make certain that the documents are consistent with the information provided by the employee in Section One" });
            context.I9Items.AddOrUpdate(i9 => i9.ID, new I9Item { ID = 19, HtmlContainer = "collapsePanelTwoInner", OrderInContainer = 6, AnswerType = "Button", Question = "For each document presented, the employer or employer�s representative records the type of document, its source and its expiration date in the appropriate area of the I-9." });
            context.I9Items.AddOrUpdate(i9 => i9.ID, new I9Item { ID = 20, HtmlContainer = "collapsePanelTwoInner", OrderInContainer = 7, AnswerType = "Button", Question = "We have a process in place and documented that ensures only authorized individuals have access to electronic versions of Form I-9." });
            context.I9Items.AddOrUpdate(i9 => i9.ID, new I9Item { ID = 21, HtmlContainer = "collapsePanelTwoInner", OrderInContainer = 8, AnswerType = "Button", Question = "We have a process in place that ensures we have appropriate back-up methods in place to protect against information loss." });
            context.I9Items.AddOrUpdate(i9 => i9.ID, new I9Item { ID = 22, HtmlContainer = "collapsePanelTwoInner", OrderInContainer = 9, AnswerType = "Button", Question = "Employees are properly trained to minimize the risk of unauthorized or accidental alteration or deletion" });
            context.I9Items.AddOrUpdate(i9 => i9.ID, new I9Item { ID = 23, HtmlContainer = "collapsePanelTwoInner", OrderInContainer = 10, AnswerType = "Button", Question = "The electronic system ensures that when an individual accesses an electronic record, the system creates a permanent record that establishes the date of access, the identity of the individual who accessed the record and the action taken." });

            // Panel Three
            context.I9Items.AddOrUpdate(i9 => i9.ID, new I9Item { ID = 24, HtmlContainer = "collapsePanelThreeInner", OrderInContainer = 1, AnswerType = "Button", Question = "Only the employee completes Section 1 of the I-9." });
            context.I9Items.AddOrUpdate(i9 => i9.ID, new I9Item { ID = 25, HtmlContainer = "collapsePanelThreeInner", OrderInContainer = 2, AnswerType = "Button", Question = "Other than for E-Verify purposes, we do not require an employee to provide a Social Security number." });
            context.I9Items.AddOrUpdate(i9 => i9.ID, new I9Item { ID = 26, HtmlContainer = "collapsePanelThreeInner", OrderInContainer = 3, AnswerType = "Button", Question = "If an employee requires assistance with Section 1 of the I-9, the person providing assistance or translation of the I-9 into another language completes the Preparer/ Translator Certification block on Form I-9" });

            // Panel Four
            context.I9Items.AddOrUpdate(i9 => i9.ID, new I9Item { ID = 27, HtmlContainer = "collapsePanelFourInner", OrderInContainer = 1, AnswerType = "Button", Question = "Only the employer completes and endorses Section 2 of the I-9" });
            context.I9Items.AddOrUpdate(i9 => i9.ID, new I9Item { ID = 28, HtmlContainer = "collapsePanelFourInner", OrderInContainer = 2, AnswerType = "Button", Question = "The employer inspects documents provided by the employee to ensure the documents reasonably appear on their face to be genuine and to relate to the person presenting them." });
            context.I9Items.AddOrUpdate(i9 => i9.ID, new I9Item { ID = 29, HtmlContainer = "collapsePanelFourInner", OrderInContainer = 3, AnswerType = "Button", Question = "The employer ensures the presented documents comply with those permitted by List A, List B and List C." });
            context.I9Items.AddOrUpdate(i9 => i9.ID, new I9Item { ID = 30, HtmlContainer = "collapsePanelFourInner", OrderInContainer = 4, AnswerType = "Button", Question = "The employer does not consider a future expiration date as a condition of employment." });

            // Panel Five
            context.I9Items.AddOrUpdate(i9 => i9.ID, new I9Item { ID = 31, HtmlContainer = "collapsePanelFiveInner", OrderInContainer = 1, AnswerType = "Button", Question = "Only the employer completes and endorses Section 3 of the I-9" });
            context.I9Items.AddOrUpdate(i9 => i9.ID, new I9Item { ID = 32, HtmlContainer = "collapsePanelFiveInner", OrderInContainer = 2, AnswerType = "Button", Question = "A �re-verification� log is maintained for employees who submit documents that have expiration dates." });
            context.I9Items.AddOrUpdate(i9 => i9.ID, new I9Item { ID = 33, HtmlContainer = "collapsePanelFiveInner", OrderInContainer = 3, AnswerType = "Button", Question = "We re-verify an employee�s right to continue to work in the United States not later than the date the employee�s employment authorization expires." });
            context.I9Items.AddOrUpdate(i9 => i9.ID, new I9Item { ID = 34, HtmlContainer = "collapsePanelFiveInner", OrderInContainer = 4, AnswerType = "Button", Question = "Unless an employee has filed an �application for an extension of stay�, if an employee cannot provide proof of continued employment authorization within three days of the expiration of documents, we would terminated that employees position with the company." });
            context.I9Items.AddOrUpdate(i9 => i9.ID, new I9Item { ID = 35, HtmlContainer = "collapsePanelFiveInner", OrderInContainer = 5, AnswerType = "Button", Question = "We complete a Form I-9 for each newly hired employee before creating a case in E-Verify." });
            context.I9Items.AddOrUpdate(i9 => i9.ID, new I9Item { ID = 36, HtmlContainer = "collapsePanelFiveInner", OrderInContainer = 6, AnswerType = "Button", Question = "The company does not request, suggest, or require a current or prospective employee to use the �self-check� component of E-Verify." });
            context.I9Items.AddOrUpdate(i9 => i9.ID, new I9Item { ID = 37, HtmlContainer = "collapsePanelFiveInner", OrderInContainer = 7, AnswerType = "Button", Question = "US-CIS compliant E-Verify posters are posted in English and Spanish in areas readily accessible by current or prospective employees." });
            context.I9Items.AddOrUpdate(i9 => i9.ID, new I9Item { ID = 38, HtmlContainer = "collapsePanelFiveInner", OrderInContainer = 8, AnswerType = "Button", Question = "US-CIS compliant Right to Work posters are posted in English and Spanish in areas readily accessible by current or prospective employees." });
            context.I9Items.AddOrUpdate(i9 => i9.ID, new I9Item { ID = 39, HtmlContainer = "collapsePanelFiveInner", OrderInContainer = 9, AnswerType = "Button", Question = "We ensure that we obtain a Social Security Number on every I-9 for each newly hired employee" });
            context.I9Items.AddOrUpdate(i9 => i9.ID, new I9Item { ID = 40, HtmlContainer = "collapsePanelFiveInner", OrderInContainer = 10, AnswerType = "Button", Question = "The company ensures that Form I-9 �List B� identity documents have a photo." });
            context.I9Items.AddOrUpdate(i9 => i9.ID, new I9Item { ID = 41, HtmlContainer = "collapsePanelFiveInner", OrderInContainer = 11, AnswerType = "Button", Question = "Create a case for each newly hired employee no later than the third business day after he or she starts work for pay." });
            context.I9Items.AddOrUpdate(i9 => i9.ID, new I9Item { ID = 42, HtmlContainer = "collapsePanelFiveInner", OrderInContainer = 12, AnswerType = "Button", Question = "Provide each employee with notice of and the opportunity to contest a Tentative Nonconfirmation (TNC)." });
            context.I9Items.AddOrUpdate(i9 => i9.ID, new I9Item { ID = 43, HtmlContainer = "collapsePanelFiveInner", OrderInContainer = 13, AnswerType = "Button", Question = "Ensure that all personally identifiable information is safeguarded." });
            context.I9Items.AddOrUpdate(i9 => i9.ID, new I9Item { ID = 44, HtmlContainer = "collapsePanelFiveInner", OrderInContainer = 14, AnswerType = "Button", Question = "We do not use E-Verify to pre-screen  an applicant for employment" });
            context.I9Items.AddOrUpdate(i9 => i9.ID, new I9Item { ID = 45, HtmlContainer = "collapsePanelFiveInner", OrderInContainer = 15, AnswerType = "Button", Question = "We do not take adverse action against an employee based on a case result unless E-Verify issues a Final Nonconfirmation." });
            context.I9Items.AddOrUpdate(i9 => i9.ID, new I9Item { ID = 46, HtmlContainer = "collapsePanelFiveInner", OrderInContainer = 16, AnswerType = "Button", Question = "We do not selectively verify the employment eligibility of a newly hired employee." });
            context.I9Items.AddOrUpdate(i9 => i9.ID, new I9Item { ID = 47, HtmlContainer = "collapsePanelFiveInner", OrderInContainer = 17, AnswerType = "Button", Question = "We do not share any user ID and/or password." });
            context.I9Items.AddOrUpdate(i9 => i9.ID, new I9Item { ID = 48, HtmlContainer = "collapsePanelFiveInner", OrderInContainer = 18, AnswerType = "Button", Question = "We do not create an E-Verify case for an employee who was hired before the employer signed the E-Verify MOU." });
            context.I9Items.AddOrUpdate(i9 => i9.ID, new I9Item { ID = 49, HtmlContainer = "collapsePanelFiveInner", OrderInContainer = 19, AnswerType = "Button", Question = "We do not discuss E-Verify results with others not needing to know." });
        }

        private void AdaItemsAddOrUpdate(WorkForce21.DAL.WFContext context)
        {
            // Panel One
            context.AdaItems.AddOrUpdate(ada => ada.ID, new AdaItem { ID = 1, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 1, AnswerType = "Button", Question = "Our employee handbook contains the summary of an employee�s rights under the ADA." });
            context.AdaItems.AddOrUpdate(ada => ada.ID, new AdaItem { ID = 2, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 2, AnswerType = "Button", Question = "We have posted in a location frequently visited by employees the appropriate ADA posters." });
            context.AdaItems.AddOrUpdate(ada => ada.ID, new AdaItem { ID = 3, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 3, AnswerType = "Button", Question = "Our employee handbook includes a description on how our company will respond to an employee with a disability." });
            context.AdaItems.AddOrUpdate(ada => ada.ID, new AdaItem { ID = 4, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 4, AnswerType = "Button", Question = "Our job advertisements include a non-discrimination ADA statement." });
            context.AdaItems.AddOrUpdate(ada => ada.ID, new AdaItem { ID = 5, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 5, AnswerType = "Button", Question = "Are policies in place to ensure that records related to disabilities are disclosed only to those with a need to know?" });
            context.AdaItems.AddOrUpdate(ada => ada.ID, new AdaItem { ID = 6, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 6, AnswerType = "Button", Question = "The company does not require any candidate for employment to take a medical examination before a offer of employment is extended." });
            context.AdaItems.AddOrUpdate(ada => ada.ID, new AdaItem { ID = 7, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 7, AnswerType = "Button", Question = "The company does not require any candidate for employment to provide any medical information or respond to medical inquires before a offer of employment is extended." });
            context.AdaItems.AddOrUpdate(ada => ada.ID, new AdaItem { ID = 8, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 8, AnswerType = "Button", Question = "The company does not require any candidate for employment to provide information about previous worker�s compensation claims before a offer of employment is extended." });
            context.AdaItems.AddOrUpdate(ada => ada.ID, new AdaItem { ID = 9, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 9, AnswerType = "Radio", Question = "From whom would you accept a request for reasonable accommodation regarding an employee or employment candidate with a disability?" });
            context.AdaItems.AddOrUpdate(ada => ada.ID, new AdaItem { ID = 10, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 10, AnswerType = "Radio", Question = "If an initial request is made for a reasonable accommodation that request may be made:" });
            context.AdaItems.AddOrUpdate(ada => ada.ID, new AdaItem { ID = 11, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 11, AnswerType = "Radio", Question = "When an initial request is made for a reasonable accommodation we require the requesting individual to provide:" });
            context.AdaItems.AddOrUpdate(ada => ada.ID, new AdaItem { ID = 12, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 12, AnswerType = "Radio", Question = "Regardless of the disability and the need reasonable accommodation (obvious or not), we would require an employee to provide documentation from a qualified medical professional" });
            context.AdaItems.AddOrUpdate(ada => ada.ID, new AdaItem { ID = 13, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 13, AnswerType = "Radio", Question = "An employee is fully recovered from an occupational injury that resulted in a temporary back impairment. The employee�s job still requires heavy lifting which caused the initial back problem. The employer fears returning the employee to the same position will cause further problems, possibly even more serious. In compliance with the ADA, we can:" });
            context.AdaItems.AddOrUpdate(ada => ada.ID, new AdaItem { ID = 14, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 14, AnswerType = "Radio", Question = "An employee has been on an approved Family Medical Leave and just exhausted their FMLA leave period. The employee can return to work and perform their job duties. However, the employee provides you with their doctors statement indicating they still need to see their physical therapist two days per week for the next 2 months after which their doctor says they should be fully recovered. The therapist only maintains office hours during the employees scheduled work days and hours. As such the employee will miss at least 12 hours of work per week and at times after the therapy the employee may need a few additional hours of rest. You will need to have another employee work overtime to cover the time lost by the recovering employee. The cost of the overtime was not part of your budget plan and the employee expected to work the overtime will do so but would prefer not to. You had expected the FML period sufficient to allow a full recovery. Under ADA your company would..." });
        }

        private void FlsaItemsAddOrUpdate(WorkForce21.DAL.WFContext context)
        {
            // Panel One
            context.FlsaItems.AddOrUpdate(flsa => flsa.ID, new FlsaItem { ID = 1, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 1, AnswerType = "Button", Question = "Employees are properly classified as �Exempt� or �Non-exempt� per the Fair Labor Standards Act requirements." });
            context.FlsaItems.AddOrUpdate(flsa => flsa.ID, new FlsaItem { ID = 2, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 2, AnswerType = "Button", Question = "The FLSA �Duties Test� is used before we arrive at a conclusion related to the employee classification." });
            context.FlsaItems.AddOrUpdate(flsa => flsa.ID, new FlsaItem { ID = 3, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 3, AnswerType = "Button", Question = "Exempt employees are paid a salary of $455 per week or greater." });
            context.FlsaItems.AddOrUpdate(flsa => flsa.ID, new FlsaItem { ID = 4, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 4, AnswerType = "Button", Question = "The company pays an exempt employee a full days wage even if the employee only works a fraction of the day." });
            context.FlsaItems.AddOrUpdate(flsa => flsa.ID, new FlsaItem { ID = 5, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 5, AnswerType = "Button", Question = "The company pays an exempt employee a full weeks wage unless the employee is unavailable for work due to illness, injury, or other employee personal reasons)." });
            context.FlsaItems.AddOrUpdate(flsa => flsa.ID, new FlsaItem { ID = 6, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 6, AnswerType = "Button", Question = "The company�s job descriptions are current, signed by the employee, accurately reflect an employee�s essential duties, and identify if the position is classified as exempt or non-exempt under FLSA." });
            context.FlsaItems.AddOrUpdate(flsa => flsa.ID, new FlsaItem { ID = 7, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 7, AnswerType = "Button", Question = "Employees are aware of their FLSA classification, their eligibility for overtime, and their rights under the FLSA." });

            // Panel Two
            context.FlsaItems.AddOrUpdate(flsa => flsa.ID, new FlsaItem { ID = 8, HtmlContainer = "collapsePanelTwoInner", OrderInContainer = 1, AnswerType = "Button", Question = "The company has an established workweek of seven (7) consecutive 24 hour periods and that work week is communicated to and known by employees." });
            context.FlsaItems.AddOrUpdate(flsa => flsa.ID, new FlsaItem { ID = 9, HtmlContainer = "collapsePanelTwoInner", OrderInContainer = 2, AnswerType = "Button", Question = "Employees are paid for time they spend preparing for work (putting on uniforms, getting tools ready, etc.) as well as ending their work day (clean up, equipment shut down, etc.)" });
            context.FlsaItems.AddOrUpdate(flsa => flsa.ID, new FlsaItem { ID = 10, HtmlContainer = "collapsePanelTwoInner", OrderInContainer = 3, AnswerType = "Button", Question = "An employee who works unauthorized overtime will be paid for the time they worked." });
            context.FlsaItems.AddOrUpdate(flsa => flsa.ID, new FlsaItem { ID = 11, HtmlContainer = "collapsePanelTwoInner", OrderInContainer = 4, AnswerType = "Button", Question = "The company has the required FLSA and state posters displayed as required (show list of poster requirements)." });
            context.FlsaItems.AddOrUpdate(flsa => flsa.ID, new FlsaItem { ID = 12, HtmlContainer = "collapsePanelTwoInner", OrderInContainer = 5, AnswerType = "Button", Question = "When computing overtime we include all payments made by the employer to or on behalf of the employee (i.e., shift differential, bonuses) during that pay period." });
            context.FlsaItems.AddOrUpdate(flsa => flsa.ID, new FlsaItem { ID = 13, HtmlContainer = "collapsePanelTwoInner", OrderInContainer = 6, AnswerType = "Button", Question = "Our employees are paid fairly and for every hour (or portion thereof) worked at the appropriate rate." });
            context.FlsaItems.AddOrUpdate(flsa => flsa.ID, new FlsaItem { ID = 14, HtmlContainer = "collapsePanelTwoInner", OrderInContainer = 7, AnswerType = "Button", Question = "It�s very rare for an employee to find errors on their paychecks." });
            context.FlsaItems.AddOrUpdate(flsa => flsa.ID, new FlsaItem { ID = 15, HtmlContainer = "collapsePanelTwoInner", OrderInContainer = 8, AnswerType = "Button", Question = "Our timekeeping system and processes work very well and our employees abide by the associated processes and rules." });
            context.FlsaItems.AddOrUpdate(flsa => flsa.ID, new FlsaItem { ID = 16, HtmlContainer = "collapsePanelTwoInner", OrderInContainer = 9, AnswerType = "Button", Question = "Non-exempt employees, when working over 40 hours per week, are paid at 1� times their regular rate of pay and cannot use �comp-time� in lieu of paid overtime. (not asked if the employer is a governmental entity)" });
            context.FlsaItems.AddOrUpdate(flsa => flsa.ID, new FlsaItem { ID = 17, HtmlContainer = "collapsePanelTwoInner", OrderInContainer = 10, AnswerType = "Button", Question = "Employees are paid (where applicable) according to the timeframe of state law requirements (click for list based on states checked in interview)." });
            context.FlsaItems.AddOrUpdate(flsa => flsa.ID, new FlsaItem { ID = 18, HtmlContainer = "collapsePanelTwoInner", OrderInContainer = 11, AnswerType = "Button", Question = "Employees are paid at least the required minimum wage per federal or state law requirements (click for list based on states checked in interview)." });
            context.FlsaItems.AddOrUpdate(flsa => flsa.ID, new FlsaItem { ID = 19, HtmlContainer = "collapsePanelTwoInner", OrderInContainer = 12, AnswerType = "Button", Question = "The company does not loan money or make advances on wages to employees." });
            context.FlsaItems.AddOrUpdate(flsa => flsa.ID, new FlsaItem { ID = 20, HtmlContainer = "collapsePanelTwoInner", OrderInContainer = 13, AnswerType = "Button", Question = "We ensure that the employees rate of pay is never below the allowable minimum wage required  by law, even after deductions from pay are made (other than employee voluntary or statutory deductions)." });
            context.FlsaItems.AddOrUpdate(flsa => flsa.ID, new FlsaItem { ID = 21, HtmlContainer = "collapsePanelTwoInner", OrderInContainer = 14, AnswerType = "Button", Question = "We have signed agreements on file with our employees for deductions we take from their pay. (Only asked if items d or beyond are checked in question 3)." });
            context.FlsaItems.AddOrUpdate(flsa => flsa.ID, new FlsaItem { ID = 22, HtmlContainer = "collapsePanelTwoInner", OrderInContainer = 15, AnswerType = "Button", Question = "The company ensures that tipped employees are paid at least the minimum required by the state(s) where we conduct business (Show state tipped list for states of operation)" });
            context.FlsaItems.AddOrUpdate(flsa => flsa.ID, new FlsaItem { ID = 23, HtmlContainer = "collapsePanelTwoInner", OrderInContainer = 16, AnswerType = "Button", Question = "The company does not claim of of the employee tips as property of the company. Tips are always the property of the employee." });

            // Panel Three
            context.FlsaItems.AddOrUpdate(flsa => flsa.ID, new FlsaItem { ID = 24, HtmlContainer = "collapsePanelThreeInner", OrderInContainer = 1, AnswerType = "Button", Question = "Hourly employees are always paid their regular rate of pay for company required breaks of 20 minutes duration or less." });
            context.FlsaItems.AddOrUpdate(flsa => flsa.ID, new FlsaItem { ID = 25, HtmlContainer = "collapsePanelThreeInner", OrderInContainer = 2, AnswerType = "Button", Question = "Hourly employees are required to record the unpaid time they are not working, such as for lunch breaks." });
            context.FlsaItems.AddOrUpdate(flsa => flsa.ID, new FlsaItem { ID = 26, HtmlContainer = "collapsePanelThreeInner", OrderInContainer = 3, AnswerType = "Button", Question = "Hourly employees are instructed that they are not to perform any work related duties during unpaid break times." });
            context.FlsaItems.AddOrUpdate(flsa => flsa.ID, new FlsaItem { ID = 27, HtmlContainer = "collapsePanelThreeInner", OrderInContainer = 4, AnswerType = "Button", Question = "Hourly employees are instructed not to perform any unpaid �off-the-clock� work when not on the work premise (such as answering phone calls, studying or reviewing work related material, maintaining work vehicles, reading and/or replying to emails, buying supplies, picking up packages, etc.)" });
            context.FlsaItems.AddOrUpdate(flsa => flsa.ID, new FlsaItem { ID = 28, HtmlContainer = "collapsePanelThreeInner", OrderInContainer = 5, AnswerType = "Button", Question = "Hourly employees comply with the requirement not to perform any unpaid �off-the-clock� work when not on the work premise." });
            context.FlsaItems.AddOrUpdate(flsa => flsa.ID, new FlsaItem { ID = 29, HtmlContainer = "collapsePanelThreeInner", OrderInContainer = 6, AnswerType = "Button", Question = "Hourly employees will be paid if they perform AUTHORIZED work related duties away from the work premises." });
            context.FlsaItems.AddOrUpdate(flsa => flsa.ID, new FlsaItem { ID = 30, HtmlContainer = "collapsePanelThreeInner", OrderInContainer = 7, AnswerType = "Button", Question = "Hourly employees will be paid if they perform NON-AUTHORIZED work related duties away from the work premises." });
            context.FlsaItems.AddOrUpdate(flsa => flsa.ID, new FlsaItem { ID = 31, HtmlContainer = "collapsePanelThreeInner", OrderInContainer = 8, AnswerType = "Button", Question = "The company provides �on-demand� breaks (paid or unpaid) for non-exempt nursing mothers allowing them adequate time to express breast milk" });
            context.FlsaItems.AddOrUpdate(flsa => flsa.ID, new FlsaItem { ID = 32, HtmlContainer = "collapsePanelThreeInner", OrderInContainer = 9, AnswerType = "Button", Question = "The company provides a private location (not a bathroom or other public space) for the purposes of expressing breast milk to nursing mothers." });
        }

        private void FmlaItemsAddOrUpdate(WorkForce21.DAL.WFContext context)
        {
            // Panel One
            context.FmlaItems.AddOrUpdate(fmla => fmla.ID, new FmlaItem { ID = 1, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 1, AnswerType = "Button", Question = "Our company has a current and comprehensive FMLA policy and provides the policy to all employees" });
            context.FmlaItems.AddOrUpdate(fmla => fmla.ID, new FmlaItem { ID = 2, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 2, AnswerType = "Button", Question = "We utilize the FMLA forms provided by the federal government or have our own FMLA forms that contain the exact same language and do not modify any provisions of the FMLA" });
            context.FmlaItems.AddOrUpdate(fmla => fmla.ID, new FmlaItem { ID = 3, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 3, AnswerType = "Button", Question = "All documents regarding an employees FMLA are stored separately from an employees personel file." });
            context.FmlaItems.AddOrUpdate(fmla => fmla.ID, new FmlaItem { ID = 4, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 4, AnswerType = "Button", Question = "We have the required FMLA posters displayed in a location frequented by employees such as a lunch room, building lobby, or break room." });
            context.FmlaItems.AddOrUpdate(fmla => fmla.ID, new FmlaItem { ID = 5, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 5, AnswerType = "Button", Question = "We have the required FMLA posters displayed in a location frequented by applicants for employment  such as in the building lobby, application area, or interviewing area." });
            context.FmlaItems.AddOrUpdate(fmla => fmla.ID, new FmlaItem { ID = 6, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 6, AnswerType = "Button", Question = "We provide employees who request leave under the FMLA a notice of the employees eligibiltiy and rights and responsibilities within five (5) business days of the employee requesting leave (Form WH-381 may be used for this purpose)." });
            context.FmlaItems.AddOrUpdate(fmla => fmla.ID, new FmlaItem { ID = 7, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 7, AnswerType = "Button", Question = "We provide employees who request leave under the FMLA a designation notie within five (5) business days  of having enough information to determine whether the leave is FMLA-qualifying (Form WH-382) may be used for this purpose)." });
            context.FmlaItems.AddOrUpdate(fmla => fmla.ID, new FmlaItem { ID = 8, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 8, AnswerType = "Button", Question = "Our company would designate an FMLA eligible employee as taking FMLA leave when" });
            context.FmlaItems.AddOrUpdate(fmla => fmla.ID, new FmlaItem { ID = 9, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 9, AnswerType = "Button", Question = "We allow FMLA requesting employees at least 15 days to return the appropriate medical certification" });
            context.FmlaItems.AddOrUpdate(fmla => fmla.ID, new FmlaItem { ID = 10, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 10, AnswerType = "Button", Question = "FMLA requesting employees who return incomplete or insufficient medical certifications are notified of the deficiencies in writing." });
            context.FmlaItems.AddOrUpdate(fmla => fmla.ID, new FmlaItem { ID = 11, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 11, AnswerType = "Button", Question = "The company will always evaluate the employees rights under the Americans with Disabilities Act (ADA) upon exhaustion of their FMLA leave to determine if an accomodation may be made due to their own medical condition." });
            context.FmlaItems.AddOrUpdate(fmla => fmla.ID, new FmlaItem { ID = 12, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 12, AnswerType = "Button", Question = "The company will generally designate an employee as being on FMLA when the conditions meet the FMLA qualifying requirements even if the employee does not request to be placed on FMLA." });
            context.FmlaItems.AddOrUpdate(fmla => fmla.ID, new FmlaItem { ID = 13, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 13, AnswerType = "Button", Question = "The company does not require an employee to expressly request FMLA leave, just a qualifying reason for the need of leave." });
            context.FmlaItems.AddOrUpdate(fmla => fmla.ID, new FmlaItem { ID = 14, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 14, AnswerType = "Button", Question = "The company's management staff understand the events that may trigger and FMLA and knows how to properly respond to those events." });
        }

        private void HPSponsorItemsAddOrUpdate(WorkForce21.DAL.WFContext context)
        {
            // Panel One
            context.HPSponsorItems.AddOrUpdate(item => item.ID, new HPSponsorItem { ID = 1, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 1, AnswerType = "Button", Question = "Is the business a governmental or recognized religious entity?" });
            context.HPSponsorItems.AddOrUpdate(item => item.ID, new HPSponsorItem { ID = 2, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 2, AnswerType = "Button", Question = "How many of your employees work Full Time (30 or more hours per week) in an average work week? (Do not include seasonal, temporary, or variable hour employees)." });
            context.HPSponsorItems.AddOrUpdate(item => item.ID, new HPSponsorItem { ID = 3, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 3, AnswerType = "Button", Question = "What is the monthly wage of your lowest paid Full Time employee?" });
            context.HPSponsorItems.AddOrUpdate(item => item.ID, new HPSponsorItem { ID = 4, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 4, AnswerType = "Button", Question = "How many of your employees work Part Time (fewer than 30 hours per week) in an average work week? (Do not include Seasonal, Temporary, or Variable Hour employees)." });
            context.HPSponsorItems.AddOrUpdate(item => item.ID, new HPSponsorItem { ID = 5, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 5, AnswerType = "Button", Question = "What is the average number of hours worked during an average work week by your Part Time employees (fewer than 30 hours per week) ?" });
            context.HPSponsorItems.AddOrUpdate(item => item.ID, new HPSponsorItem { ID = 6, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 6, AnswerType = "Button", Question = "In an average year, how many Seasonal, Temporary or Variable Hour employees will you hire to work 120 or more consecutive calendar days per year?" });
            context.HPSponsorItems.AddOrUpdate(item => item.ID, new HPSponsorItem { ID = 7, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 7, AnswerType = "Button", Question = "Of those Seasonal, Temporary or Variable Hour employees, how many will work 30 or more hours during an average work week?" });
            context.HPSponsorItems.AddOrUpdate(item => item.ID, new HPSponsorItem { ID = 8, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 8, AnswerType = "Button", Question = "Do you offer an employer sponsored health insurance plan to any of your employees?" });
            context.HPSponsorItems.AddOrUpdate(item => item.ID, new HPSponsorItem { ID = 9, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 9, AnswerType = "Button", Question = "Does the plan meet \"ACA Minimum Value\" requirements? If unsure, check with your carrier or broker." });
            context.HPSponsorItems.AddOrUpdate(item => item.ID, new HPSponsorItem { ID = 10, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 10, AnswerType = "Button", Question = "Is your plan considered \"grandfathered\" under the provisions of the PPACA?" });
            context.HPSponsorItems.AddOrUpdate(item => item.ID, new HPSponsorItem { ID = 11, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 11, AnswerType = "Button", Question = "How many hours must employees work per week to be eligible for your employer sponsored health insurance plan?" });
            context.HPSponsorItems.AddOrUpdate(item => item.ID, new HPSponsorItem { ID = 12, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 12, AnswerType = "Button", Question = "How many of the Full Time employees are eligible to join your health insurance plan? (If you do not offer health insurance, enter 0)." });
            context.HPSponsorItems.AddOrUpdate(item => item.ID, new HPSponsorItem { ID = 13, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 13, AnswerType = "Button", Question = "How many of your eligible Full Time Employees participate in the health plan ? (If you do not offer of health insurance, enter 0)." });
            context.HPSponsorItems.AddOrUpdate(item => item.ID, new HPSponsorItem { ID = 14, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 14, AnswerType = "Button", Question = "What is the employee monthly premium cost for your lowest cost \"Single only\" insurance offering that meets \"ACA Minimum Value\" requirements?" });
            context.HPSponsorItems.AddOrUpdate(item => item.ID, new HPSponsorItem { ID = 15, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 15, AnswerType = "Button", Question = "After hire, how many days must an employee wait before becoming eligible to join the employers health plan?" });
            context.HPSponsorItems.AddOrUpdate(item => item.ID, new HPSponsorItem { ID = 16, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 16, AnswerType = "Button", Question = "Are eligible employees enrolled in an insurance plan the first day of the next month or the day after they meet the waiting day period?" });
            context.HPSponsorItems.AddOrUpdate(item => item.ID, new HPSponsorItem { ID = 17, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 17, AnswerType = "Button", Question = "Are adult children under 26 years of age permitted to remain on, or gain entry to the parental  plan without any other requirement?" });
            context.HPSponsorItems.AddOrUpdate(item => item.ID, new HPSponsorItem { ID = 18, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 18, AnswerType = "Button", Question = "Do all classes of employees have the same enrollment eligibility requirements?" });
            context.HPSponsorItems.AddOrUpdate(item => item.ID, new HPSponsorItem { ID = 19, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 19, AnswerType = "Button", Question = "Do all employees have the same benefits within the same plan or do benefit levels vary by employee class?" });
            context.HPSponsorItems.AddOrUpdate(item => item.ID, new HPSponsorItem { ID = 20, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 20, AnswerType = "Button", Question = "Does the employer contribute equally to all classes of employees or do the contributions vary depending on employee type or class?" });
            context.HPSponsorItems.AddOrUpdate(item => item.ID, new HPSponsorItem { ID = 21, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 21, AnswerType = "Button", Question = "Is this plan \"fully Insured\" (claims paid by the insurance carriers assets) or \"self funded\" (claims paid from the employers/sponsors assets)?" });
            context.HPSponsorItems.AddOrUpdate(item => item.ID, new HPSponsorItem { ID = 22, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 22, AnswerType = "Button", Question = "Do you offer a Flexible Spending Account (FSA) or a Health Reimbursement Arrangement (HRA) to your employees?" });
            context.HPSponsorItems.AddOrUpdate(item => item.ID, new HPSponsorItem { ID = 23, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 23, AnswerType = "Button", Question = "Do you limit the contribution to the FSA to a maximum of $2,550 per year?" });
            context.HPSponsorItems.AddOrUpdate(item => item.ID, new HPSponsorItem { ID = 24, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 24, AnswerType = "Button", Question = "Are employee health plan premiums paid pre-tax or post tax from the employee's wage?" });
            context.HPSponsorItems.AddOrUpdate(item => item.ID, new HPSponsorItem { ID = 25, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 25, AnswerType = "Button", Question = "Do you conduct an annual non-discrimination test?" });
            context.HPSponsorItems.AddOrUpdate(item => item.ID, new HPSponsorItem { ID = 26, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 26, AnswerType = "Button", Question = "If your plan is considered \"grandfathered\", are you providing the required disclosures to employees and performing the recordkeeping requirements?" });
            context.HPSponsorItems.AddOrUpdate(item => item.ID, new HPSponsorItem { ID = 27, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 27, AnswerType = "Button", Question = "Do you distribute the \"Marketplace Coverage Notice\" to every employee and provide it to new employees at time of hire?" });
            context.HPSponsorItems.AddOrUpdate(item => item.ID, new HPSponsorItem { ID = 28, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 28, AnswerType = "Button", Question = "Do you provide a copy of the Summary of Benefits Coverage (SBC) to a new participant upon initial eligibility, to all participants at open enrollment,�and to a participant upon request?" });
            context.HPSponsorItems.AddOrUpdate(item => item.ID, new HPSponsorItem { ID = 29, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 29, AnswerType = "Button", Question = "Do you have a ERISA compliant Summary Plan Description and has it been updated to be in compliance with the PPACA?" });
            context.HPSponsorItems.AddOrUpdate(item => item.ID, new HPSponsorItem { ID = 30, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 30, AnswerType = "Button", Question = "Are you using the updated COBRA notice informing eligible employees of their right to enroll in a Marketplace plan?" });
            context.HPSponsorItems.AddOrUpdate(item => item.ID, new HPSponsorItem { ID = 31, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 31, AnswerType = "Button", Question = "Have you started to report the cost of insurance on your employee's W2 form?" });
            context.HPSponsorItems.AddOrUpdate(item => item.ID, new HPSponsorItem { ID = 32, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 32, AnswerType = "Button", Question = "Does the business file an annual Form 5500 with the Department of Labor for its Health Plan?" });
        }

        private void ContractorItemsAddOrUpdate(WorkForce21.DAL.WFContext context)
        {
            // Panel One
            context.ContractorItems.AddOrUpdate(item => item.ID, new ContractorItem { ID = 1, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 1, AnswerType = "Button", Question = "The company allows it�s contractors to determine the methods necessary to achieve the work objective. The company does not provide significant direction to the contractor." });
            context.ContractorItems.AddOrUpdate(item => item.ID, new ContractorItem { ID = 2, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 2, AnswerType = "Button", Question = "The company does not provide its contractors with training on how to complete their work requirements." });
            context.ContractorItems.AddOrUpdate(item => item.ID, new ContractorItem { ID = 3, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 3, AnswerType = "Button", Question = "Generally, the company does not set the contractors specific work hours or days. The accomplishment of their assignment is up to them." });
            context.ContractorItems.AddOrUpdate(item => item.ID, new ContractorItem { ID = 4, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 4, AnswerType = "Button", Question = "The company does not reimburse its contractors for expenses incurred in their work with us." });
            context.ContractorItems.AddOrUpdate(item => item.ID, new ContractorItem { ID = 5, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 5, AnswerType = "Button", Question = "The company does not provide its contractors with employee benefits." });
            context.ContractorItems.AddOrUpdate(item => item.ID, new ContractorItem { ID = 6, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 6, AnswerType = "Button", Question = "The company does not instruct its contractors on where to purchase supplies, what vendors to use, or who should assist them if needed." });
            context.ContractorItems.AddOrUpdate(item => item.ID, new ContractorItem { ID = 7, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 7, AnswerType = "Button", Question = "The company enters into written contracts/agreements with its contractors." });
            context.ContractorItems.AddOrUpdate(item => item.ID, new ContractorItem { ID = 8, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 8, AnswerType = "Button", Question = "The company does not withhold any taxes, social security, or Medicare payments from the contractor�s fees." });
            context.ContractorItems.AddOrUpdate(item => item.ID, new ContractorItem { ID = 9, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 9, AnswerType = "Button", Question = "The company reports the amount paid to its contractors using a form 1099-MISC, never a W2." });
            context.ContractorItems.AddOrUpdate(item => item.ID, new ContractorItem { ID = 10, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 10, AnswerType = "Button", Question = "Our contractors are free to accept business with other companies or organizations." });
            context.ContractorItems.AddOrUpdate(item => item.ID, new ContractorItem { ID = 11, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 11, AnswerType = "Button", Question = "Our contractors have established a legal tax entity and their fees are reported to the IRS using the EIN of the tax entity." });
            context.ContractorItems.AddOrUpdate(item => item.ID, new ContractorItem { ID = 12, HtmlContainer = "collapsePanelOneInner", OrderInContainer = 12, AnswerType = "Button", Question = "The company is well prepared and can readily respond to a Department of Labor WHD notice of audit." });
        }

        private void CustomAnswersAddOrUpdate(WorkForce21.DAL.WFContext context)
        {
            // I9
            context.CustomAnswers.AddOrUpdate(ca => ca.ID, new CustomAnswer { ID = 1, Answer = "Paper Only" });
            context.CustomAnswers.AddOrUpdate(ca => ca.ID, new CustomAnswer { ID = 2, Answer = "Electronic Only" });
            context.CustomAnswers.AddOrUpdate(ca => ca.ID, new CustomAnswer { ID = 3, Answer = "Both" });

            I9Item i9Item = context.I9Items.Find(1);
            i9Item.CustomAnswers.Add(context.CustomAnswers.Find(1));
            i9Item.CustomAnswers.Add(context.CustomAnswers.Find(2));
            i9Item.CustomAnswers.Add(context.CustomAnswers.Find(3));

            // ADA
            context.CustomAnswers.AddOrUpdate(ca => ca.ID, new CustomAnswer { ID = 4, Answer = "The employee or candidate for employment, a family member, friend, or health professional." });
            context.CustomAnswers.AddOrUpdate(ca => ca.ID, new CustomAnswer { ID = 5, Answer = "Only the employee or the candidate for employment" });
            context.CustomAnswers.AddOrUpdate(ca => ca.ID, new CustomAnswer { ID = 6, Answer = "Only the employee or an immediate family member (spouse, parent, legal age child)" });
            context.CustomAnswers.AddOrUpdate(ca => ca.ID, new CustomAnswer { ID = 7, Answer = "Only the employee or candidate for employment" });
            context.CustomAnswers.AddOrUpdate(ca => ca.ID, new CustomAnswer { ID = 8, Answer = "The employee or the employee�s immediate supervisor." });
            context.CustomAnswers.AddOrUpdate(ca => ca.ID, new CustomAnswer { ID = 9, Answer = "Only in writing, in  a format chosen by the individual making the request" });
            context.CustomAnswers.AddOrUpdate(ca => ca.ID, new CustomAnswer { ID = 10, Answer = "Only in writing using a format chosen by the company" });
            context.CustomAnswers.AddOrUpdate(ca => ca.ID, new CustomAnswer { ID = 11, Answer = "Verbally or in writing" });
            context.CustomAnswers.AddOrUpdate(ca => ca.ID, new CustomAnswer { ID = 12, Answer = "The specific type of accommodation being requested. For example, an employee who has a back problem and needs help periodically lifting heavy items would need to state how the problem can be fixed-for example �I need a power lift to assist me�." });
            context.CustomAnswers.AddOrUpdate(ca => ca.ID, new CustomAnswer { ID = 13, Answer = "A note from a qualified medical professional identifying the need for an accommodation." });
            context.CustomAnswers.AddOrUpdate(ca => ca.ID, new CustomAnswer { ID = 14, Answer = "Just a mention of the problem such as �I�m having a problem getting to work on time due to medical treatments I need to undergo.�" });
            context.CustomAnswers.AddOrUpdate(ca => ca.ID, new CustomAnswer { ID = 15, Answer = "A note from their supervisor indicating that the employee is unable to perform the essential duties of their position." });
            context.CustomAnswers.AddOrUpdate(ca => ca.ID, new CustomAnswer { ID = 16, Answer = "Yes" });
            context.CustomAnswers.AddOrUpdate(ca => ca.ID, new CustomAnswer { ID = 17, Answer = "No" });
            context.CustomAnswers.AddOrUpdate(ca => ca.ID, new CustomAnswer { ID = 18, Answer = "Not ask any further questions about the employee�s ability to do the job and return them to their former position" });
            context.CustomAnswers.AddOrUpdate(ca => ca.ID, new CustomAnswer { ID = 19, Answer = "Discuss with the employee their ability to do the work they had been doing prior to the injury." });
            context.CustomAnswers.AddOrUpdate(ca => ca.ID, new CustomAnswer { ID = 20, Answer = "Send the employee to another doctor for a second opinion" });
            context.CustomAnswers.AddOrUpdate(ca => ca.ID, new CustomAnswer { ID = 21, Answer = "Require the employee to obtain a second opinion before returning them to work-they no longer have FMLA job protection" });
            context.CustomAnswers.AddOrUpdate(ca => ca.ID, new CustomAnswer { ID = 22, Answer = "Disallow the request-the FMLA job protection is over and the overtime cost was not planned and will impact another employee." });
            context.CustomAnswers.AddOrUpdate(ca => ca.ID, new CustomAnswer { ID = 23, Answer = "Allow the employee to return to a shortened work week and pay the additional overtime cost." });
            context.CustomAnswers.AddOrUpdate(ca => ca.ID, new CustomAnswer { ID = 24, Answer = "Require the employee to remain on leave until they can return to their position and work all the required hours." });

            AdaItem adaItem = context.AdaItems.Find(9);
            adaItem.CustomAnswers.Add(context.CustomAnswers.Find(4));
            adaItem.CustomAnswers.Add(context.CustomAnswers.Find(5));
            adaItem.CustomAnswers.Add(context.CustomAnswers.Find(6));
            adaItem.CustomAnswers.Add(context.CustomAnswers.Find(7));
            adaItem.CustomAnswers.Add(context.CustomAnswers.Find(8));
            adaItem = context.AdaItems.Find(10);
            adaItem.CustomAnswers.Add(context.CustomAnswers.Find(9));
            adaItem.CustomAnswers.Add(context.CustomAnswers.Find(10));
            adaItem.CustomAnswers.Add(context.CustomAnswers.Find(11));
            adaItem = context.AdaItems.Find(11);
            adaItem.CustomAnswers.Add(context.CustomAnswers.Find(12));
            adaItem.CustomAnswers.Add(context.CustomAnswers.Find(13));
            adaItem.CustomAnswers.Add(context.CustomAnswers.Find(14));
            adaItem.CustomAnswers.Add(context.CustomAnswers.Find(15));
            adaItem = context.AdaItems.Find(12);
            adaItem.CustomAnswers.Add(context.CustomAnswers.Find(16));
            adaItem.CustomAnswers.Add(context.CustomAnswers.Find(17));
            adaItem = context.AdaItems.Find(13);
            adaItem.CustomAnswers.Add(context.CustomAnswers.Find(18));
            adaItem.CustomAnswers.Add(context.CustomAnswers.Find(19));
            adaItem.CustomAnswers.Add(context.CustomAnswers.Find(20));
            adaItem = context.AdaItems.Find(14);
            adaItem.CustomAnswers.Add(context.CustomAnswers.Find(21));
            adaItem.CustomAnswers.Add(context.CustomAnswers.Find(22));
            adaItem.CustomAnswers.Add(context.CustomAnswers.Find(23));
            adaItem.CustomAnswers.Add(context.CustomAnswers.Find(24));

            // FLSA
            // ADEA
            // FMLA
            // HP Sponsor
            // Contractor
        }

        private void ButtonOptionsAddOrUpdate(WorkForce21.DAL.WFContext context)
        {
            context.ButtonOptions.AddOrUpdate(e => e.ID, new ButtonOption { ID = 1, Text = "Strongly Agree", Points = 2 });
            context.ButtonOptions.AddOrUpdate(e => e.ID, new ButtonOption { ID = 2, Text = "Agree", Points = 1 });
            context.ButtonOptions.AddOrUpdate(e => e.ID, new ButtonOption { ID = 3, Text = "Disagree", Points = -1 });
            context.ButtonOptions.AddOrUpdate(e => e.ID, new ButtonOption { ID = 4, Text = "Strongly Disagree", Points = -2 });
        }
    }
}
