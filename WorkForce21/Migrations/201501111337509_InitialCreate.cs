namespace WorkForce21.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Clients",
                c => new
                    {
                        cliID = c.Int(nullable: false, identity: true),
                        cliCompany = c.String(nullable: false, maxLength: 50, unicode: false),
                        cliFirstName = c.String(nullable: false, maxLength: 50, unicode: false),
                        cliLastName = c.String(nullable: false, maxLength: 50, unicode: false),
                        cliPhone = c.String(maxLength: 20, unicode: false),
                        cliEmail = c.String(nullable: false, maxLength: 50, unicode: false),
                        cliPassword = c.String(nullable: false, maxLength: 100, unicode: false),
                        cliAdmin = c.Boolean(nullable: false),
                        cliActive = c.Boolean(nullable: false),
                        cliModifiedBy = c.String(nullable: false, maxLength: 50, unicode: false),
                        cliModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.cliID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Clients");
        }
    }
}
