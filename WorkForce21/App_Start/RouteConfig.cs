using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Routing;
using Microsoft.AspNet.FriendlyUrls;

namespace WorkForce21
{
    public static class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.EnableFriendlyUrls();
            routes.MapPageRoute("Administration", "Administration", "~/Views/Administration.aspx");
            routes.MapPageRoute("About", "About", "~/Views/About.aspx");
            routes.MapPageRoute("Questionnaire", "Questionnaire", "~/Views/Default.aspx");
            routes.MapPageRoute("I9", "I9", "~/Views/I9.aspx");
            routes.MapPageRoute("I9Edit", "I9Edit", "~/Views/I9.aspx");
            routes.MapPageRoute("ADA", "ADA", "~/Views/Ada.aspx");
            routes.MapPageRoute("ADAEdit", "ADAEdit", "~/Views/Ada.aspx");
            routes.MapPageRoute("FLSA", "FLSA", "~/Views/Flsa.aspx");
            routes.MapPageRoute("FLSAEdit", "FLSAEdit", "~/Views/Flsa.aspx");
            routes.MapPageRoute("FMLA", "FMLA", "~/Views/Fmla.aspx");
            routes.MapPageRoute("FMLAEdit", "FMLAEdit", "~/Views/Fmla.aspx");
            routes.MapPageRoute("HPSponsor", "HPSponsor", "~/Views/HPSponsor.aspx");
            routes.MapPageRoute("HPSponsorEdit", "HPSponsorEdit", "~/Views/HPSponsor.aspx");
            routes.MapPageRoute("Contractor", "Contractor", "~/Views/Contractor.aspx");
            routes.MapPageRoute("ContractorEdit", "ContractorEdit", "~/Views/Contractor.aspx");
            routes.MapPageRoute("Profile", "Profile", "~/Views/Account.aspx");
            routes.MapPageRoute("Login", "Login", "~/Public/Login.aspx");
        }
    }
}
