﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WorkForce21.Models
{
    public class Questionnaire
    {
        [Key]
        [Column("queID")]
        public int ID { get; set; }

        [Column("queClientID")]
        public int ClientID { get; set; }

        [Column("queAuditBeginDate")]
        public DateTime AuditBeginDate { get; set; }

        [Column("queLastModifiedDate")]
        public DateTime LastModifiedDate { get; set; }

        [Column("queEE50OrMore")]
        public bool EE50OrMore { get; set; }

        [Column("queEE20_49")]
        public bool EE20_49 { get; set; }

        [Column("queEE15_19")]
        public bool EE15_19 { get; set; }

        [Column("queEELessThan15")]
        public bool EELessThan15 { get; set; }

        [Column("queOfferHealthInsurance")]
        public bool OfferHealthInsurance { get; set; }

        [Column("queContractors1099")]
        public bool Contractors1099 { get; set; }
    }
}