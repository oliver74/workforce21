﻿using System.Collections.Generic;

namespace WorkForce21.Models
{
    public class FmlaItem : BaseItem
    {
        public virtual IList<CustomAnswer> CustomAnswers { get; set; }
    }
}