﻿using System.Collections.Generic;

namespace WorkForce21.Models
{
    public class AdaItem : BaseItem
    {
        public virtual IList<CustomAnswer> CustomAnswers { get; set; }
    }
}