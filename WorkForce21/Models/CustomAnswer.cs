﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WorkForce21.Models
{
    public class CustomAnswer
    {
        [Key]
        [Column("canID")]
        public int ID { get; set; }

        [Required]
        [Column("canAnswer", TypeName = "varchar")]
        [MaxLength]
        public string Answer { get; set; }
    }
}