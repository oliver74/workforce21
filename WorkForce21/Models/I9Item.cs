﻿using System.Collections.Generic;

namespace WorkForce21.Models
{
    public class I9Item : BaseItem
    {
        public virtual IList<CustomAnswer> CustomAnswers { get; set; }
    }
}