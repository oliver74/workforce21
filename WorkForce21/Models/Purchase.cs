﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WorkForce21.Models
{
    public class Purchase
    {
        [Key]
        [Column("pucID")]
        public int ID { get; set; }

        [Column("pucClientID")]
        public int ClientID { get; set; }

        [Required]
        [Column("pucForms", TypeName = "varchar")]
        [MaxLength(40)]
        public string Forms { get; set; }

        [Column("pucDate")]
        public DateTime Date { get; set; }

        [Column("pucReferentNum", TypeName = "varchar")]
        [MaxLength(30)]
        public string ReferentNum { get; set; }

        [Column("pucCompleted")]
        public bool Completed { get; set; }

        [Column("pucMessage", TypeName = "varchar")]
        [MaxLength]
        public string Message { get; set; }
    }
}