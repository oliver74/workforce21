﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WorkForce21.Models
{
    public class Client
    {
        [Key]
        [Column("cliID")]
        public int ID { get; set; }

        [Column("cliCompany", TypeName = "varchar")]
        [MaxLength(50)]
        public string Company { get; set; }

        [Column("cliFirstName", TypeName = "varchar")]
        [MaxLength(50)]
        public string FirstName { get; set; }

        [Column("cliLastName", TypeName = "varchar")]
        [MaxLength(50)]
        public string LastName { get; set; }

        [Column("cliPhone", TypeName = "varchar")]
        [MaxLength(20)]
        public string Phone { get; set; }

        [Required]
        [Column("cliEmail", TypeName = "varchar")]
        [MaxLength(50)]
        public string Email { get; set; }

        [Required]
        [Column("cliPassword", TypeName = "varchar")]
        [MaxLength(100)]
        public string Password { get; set; }

        [Column("cliAdmin")]
        public bool Admin { get; set; }

        [Column("cliActive")]
        public bool Active { get; set; }

        [Required]
        [Column("cliModifiedBy", TypeName = "varchar")]
        [MaxLength(50)]
        public string ModifiedBy { get; set; }

        [Column("cliModifiedDate")]
        public DateTime ModifiedDate { get; set; }

        [Column("cliAddress", TypeName = "varchar")]
        [MaxLength(50)]
        public string Address { get; set; }

        [Column("cliCity", TypeName = "varchar")]
        [MaxLength(30)]
        public string City { get; set; }

        [Column("cliPZip", TypeName = "varchar")]
        [MaxLength(10)]
        public string Zip { get; set; }
    }
}