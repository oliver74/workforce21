﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WorkForce21.Models
{
    public class BaseItem
    {
        [Key]
        [Column("itemID")]
        public int ID { get; set; }

        [Required]
        [Column("itemHtmlContainer", TypeName = "varchar")]
        [MaxLength(30)]
        public string HtmlContainer { get; set; }

        [Column("itemOrderInContainer")]
        public int OrderInContainer { get; set; }

        [Required]
        [Column("itemAnswerType", TypeName = "varchar")]
        [MaxLength(10)]
        public string AnswerType { get; set; }

        [Required]
        [Column("itemQuestion", TypeName = "varchar")]
        [MaxLength]
        public string Question { get; set; }

        [NotMapped]
        public int Answer { get; set; }
    }
}