﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WorkForce21.Models
{
    public class ButtonOption
    {
        [Key]
        [Column("bopID")]
        public int ID { get; set; }

        [Required]
        [Column("bopText", TypeName = "varchar")]
        [MaxLength(20)]
        public string Text { get; set; }

        [Column("bopPoints")]
        public int Points { get; set; }
    }
}