﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WorkForce21.Models
{
    public class BaseAnswer
    {
        [Key]
        [Column("ansClientID", Order = 0)]
        public int ClientID { get; set; }

        [Key]
        [Column("ansItemID", Order = 1)]
        public int ItemID { get; set; }

        [Column("ansAnswer")]
        public int Answer { get; set; }
    }
}