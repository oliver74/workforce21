﻿using System.Collections.Generic;

namespace WorkForce21.Models
{
    public class ContractorItem : BaseItem
    {
        public virtual IList<CustomAnswer> CustomAnswers { get; set; }
    }
}