﻿function showMessage(msg, life, title, dialogClass) {
    if (life == '' || life == null) life = 5000;
    if (title == '' || title == null) title = 'Error!';
    if (dialogClass == '' || dialogClass == null) dialogClass = 'alert-danger';
    switch (alertmethod[0].toString().toUpperCase()) {
        case 'GROWL':
        default:
            $.jGrowl(msg, { life: life });
            break;
        case 'ALERT':
            $.alerts.dialogClass = dialogClass;
            jAlert(msg, title, function () {
                $.alerts.dialogClass = null; // reset to default
            });
            break;
    }
}

//function buildMainMenu() {
//    if (user[0].Views != null) {
//        if ((user[0].Views & 1) == 0) $('#liCompliance').hide();
//        if ((user[0].Views & 2) == 0) $('#liCalculator').hide();
//        if ((user[0].Views & 4) == 0) $('#liGator').hide();
//    }
//}

var baseUrl = '/workforce21';

function getClients(callback) {
    $("body").css("cursor", "wait");
    $.ajax({
        url: baseUrl + '/administration/clients',
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            callback.call(this, data);
            $("body").css("cursor", "default");
        },
        error: function () {
            $("body").css("cursor", "default");
            showMessage('Could not retrieve clients');
        }
    });
}

function postAddClientSave(callback, client) {
    $("body").css("cursor", "wait");
    $.ajax({
        url: baseUrl + '/administration/clients',
        data: client,
        type: 'POST',
        dataType: 'json',
        success: function (data) {
            callback.call(this, data);
            $("body").css("cursor", "default");
        },
        error: function () {
            $("body").css("cursor", "default");
            showMessage('Could not add the client.');
        }
    });
}

function putEditClientSave(callback, client, id) {
    $("body").css("cursor", "wait");
    $.ajax({
        url: baseUrl + '/administration/clients/' + id,
        data: client,
        type: 'PUT',
        dataType: 'json',
        success: function (data) {
            callback.call(this, data);
            $("body").css("cursor", "default");
        },
        error: function () {
            $("body").css("cursor", "default");
            showMessage('Could not modify the client.');
        }
    });
}

function postClientDelete(callback, id) {
    $("body").css("cursor", "wait");
    $.ajax({
        url: baseUrl + '/administration/clients/' + id,
        type: 'DELETE',
        dataType: 'json',
        success: function (data) {
            callback.call(this, data);
            $("body").css("cursor", "default");
        },
        error: function () {
            $("body").css("cursor", "default");
            showMessage('Could not delete the client.');
        }
    });
}

function updateProfile(callback, id, firstname, lastname) {
    $("body").css("cursor", "wait");
    $.ajax({
        url: baseUrl + '/administration/clients/updateprofile',
        data: {
            id: id,
            firstname: firstname,
            lastname: lastname
        },
        type: 'POST',
        dataType: 'json',
        success: function (data) {
            callback.call(this, data);
            $("body").css("cursor", "default");
        },
        error: function () {
            $("body").css("cursor", "default");
            showMessage('Experienced an error updating your profile.');
        }
    });
}

function changePassword(callback, id, oldpassword, newpassword) {
    $("body").css("cursor", "wait");
    $.ajax({
        url: baseUrl + '/administration/clients/changepassword',
        data: {
            id: id,
            oldpassword: oldpassword,
            newpassword: newpassword
        },
        type: 'POST',
        dataType: 'json',
        success: function (data) {
            callback.call(this, data);
            $("body").css("cursor", "default");
        },
        error: function () {
            $("body").css("cursor", "default");
            showMessage('Experienced an error changing your password.');
        }
    });
}

// ------------------------ Questionnaires --------------------------------

function postQuestionnaire(callback, id, questionnaire) {
    $("body").css("cursor", "wait");
    $.ajax({
        url: baseUrl + '/clients/' + id + '/questionnaire',
        data: questionnaire,
        type: 'POST',
        dataType: 'json',
        success: function (data) {
            callback.call(this, data);
            $("body").css("cursor", "default");
        },
        error: function () {
            $("body").css("cursor", "default");
            showMessage('Could not submit the questionnaire.');
        }
    });
}

function getQuestionnaire(callback, id) {
    $("body").css("cursor", "wait");
    $.ajax({
        url: baseUrl + '/clients/' + id + '/questionnaire',
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            callback.call(this, data);
            $("body").css("cursor", "default");
        },
        error: function () {
            $("body").css("cursor", "default");
            showMessage('Could not retrieve questionnaire');
        }
    });
}

function getI9Items(callback) {
    $("body").css("cursor", "wait");
    var url = baseUrl + '/i9items';
    if ($(location).attr('pathname').indexOf('Edit') != -1) url += 'edit';
    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            callback.call(this, data);
            $("body").css("cursor", "default");
        },
        error: function () {
            $("body").css("cursor", "default");
            showMessage('Could not retrieve I9 items');
        }
    });
}

function getAdaItems(callback) {
    $("body").css("cursor", "wait");
    var url = baseUrl + '/adaitems';
    if ($(location).attr('pathname').indexOf('Edit') != -1) url += 'edit';
    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            callback.call(this, data);
            $("body").css("cursor", "default");
        },
        error: function () {
            $("body").css("cursor", "default");
            showMessage('Could not retrieve ADA items');
        }
    });
}

function getFlsaItems(callback) {
    $("body").css("cursor", "wait");
    var url = baseUrl + '/flsaitems';
    if ($(location).attr('pathname').indexOf('Edit') != -1) url += 'edit';
    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            callback.call(this, data);
            $("body").css("cursor", "default");
        },
        error: function () {
            $("body").css("cursor", "default");
            showMessage('Could not retrieve FLSA items');
        }
    });
}

function getFmlaItems(callback) {
    $("body").css("cursor", "wait");
    var url = baseUrl + '/fmlaitems';
    if ($(location).attr('pathname').indexOf('Edit') != -1) url += 'edit';
    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            callback.call(this, data);
            $("body").css("cursor", "default");
        },
        error: function () {
            $("body").css("cursor", "default");
            showMessage('Could not retrieve FMLA items');
        }
    });
}

function getHPSponsorItems(callback) {
    $("body").css("cursor", "wait");
    var url = baseUrl + '/hpsponsoritems';
    if ($(location).attr('pathname').indexOf('Edit') != -1) url += 'edit';
    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            callback.call(this, data);
            $("body").css("cursor", "default");
        },
        error: function () {
            $("body").css("cursor", "default");
            showMessage('Could not retrieve Health Plan Sponsor items');
        }
    });
}

function getContractorItems(callback) {
    $("body").css("cursor", "wait");
    var url = baseUrl + '/contractoritems';
    if ($(location).attr('pathname').indexOf('Edit') != -1) url += 'edit';
    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            callback.call(this, data);
            $("body").css("cursor", "default");
        },
        error: function () {
            $("body").css("cursor", "default");
            showMessage('Could not retrieve Contractor items');
        }
    });
}

function postAdaSubmit(callback, itemid, answerid) {
    $("body").css("cursor", "wait");
    $.ajax({
        url: baseUrl + '/adaitems',
        type: 'POST',
        data : { "ItemID" : itemid, "AnswerID" : answerid },
        dataType: 'json',
        success: function (data) {
            callback.call(this, data);
            $("body").css("cursor", "default");
        },
        error: function () {
            $("body").css("cursor", "default");
            showMessage('Could not submit the answer');
        }
    });
}

function postContractorSubmit(callback, itemid, answerid) {
    $("body").css("cursor", "wait");
    $.ajax({
        url: baseUrl + '/contractoritems',
        type: 'POST',
        data: { "ItemID": itemid, "AnswerID": answerid },
        dataType: 'json',
        success: function (data) {
            callback.call(this, data);
            $("body").css("cursor", "default");
        },
        error: function () {
            $("body").css("cursor", "default");
            showMessage('Could not submit the answer');
        }
    });
}

function postFlsaSubmit(callback, itemid, answerid) {
    $("body").css("cursor", "wait");
    $.ajax({
        url: baseUrl + '/flsaitems',
        type: 'POST',
        data: { "ItemID": itemid, "AnswerID": answerid },
        dataType: 'json',
        success: function (data) {
            callback.call(this, data);
            $("body").css("cursor", "default");
        },
        error: function () {
            $("body").css("cursor", "default");
            showMessage('Could not submit the answer');
        }
    });
}

function postFmlaSubmit(callback, itemid, answerid) {
    $("body").css("cursor", "wait");
    $.ajax({
        url: baseUrl + '/fmlaitems',
        type: 'POST',
        data: { "ItemID": itemid, "AnswerID": answerid },
        dataType: 'json',
        success: function (data) {
            callback.call(this, data);
            $("body").css("cursor", "default");
        },
        error: function () {
            $("body").css("cursor", "default");
            showMessage('Could not submit the answer');
        }
    });
}

function postHPSponsorSubmit(callback, itemid, answerid) {
    $("body").css("cursor", "wait");
    $.ajax({
        url: baseUrl + '/hpsponsoritems',
        type: 'POST',
        data: { "ItemID": itemid, "AnswerID": answerid },
        dataType: 'json',
        success: function (data) {
            callback.call(this, data);
            $("body").css("cursor", "default");
        },
        error: function () {
            $("body").css("cursor", "default");
            showMessage('Could not submit the answer');
        }
    });
}

function postI9Submit(callback, itemid, answerid) {
    $("body").css("cursor", "wait");
    $.ajax({
        url: baseUrl + '/i9items',
        type: 'POST',
        data: { "ItemID": itemid, "AnswerID": answerid },
        dataType: 'json',
        success: function (data) {
            callback.call(this, data);
            $("body").css("cursor", "default");
        },
        error: function () {
            $("body").css("cursor", "default");
            showMessage('Could not submit the answer');
        }
    });
}

function postPurchaseSubmit(callback, items) {
    $("body").css("cursor", "wait");
    $.ajax({
        url: baseUrl + '/administration/purchases/',
        type: 'POST',
        data: '=' + items,
        dataType: 'json',
        success: function (data) {
            callback.call(this, data);
            $("body").css("cursor", "default");
        },
        error: function () {
            $("body").css("cursor", "default");
            showMessage('Could not initiate the purchase');
        }
    });
}
