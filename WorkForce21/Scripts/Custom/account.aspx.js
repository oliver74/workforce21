﻿$(document).ready(function () {
    //$('#mastertitle').text('Account Settings');
    //if (!isadmin[0]) $('#liAdvisors').hide();
    //buildMainMenu();
    accountpnl();
});

function accountpnl() {
    accountData = accountData[0];
    $('#txtLoginName').val(accountData.Email);
    $('#txtFirstName').val(accountData.FirstName);
    $('#txtLastName').val(accountData.LastName);

    $('#aUpdatePassword').unbind('click').click(function () {
        $('#changePassword').modal('show');
        return false;
    });

    $('#cmdChangePassword').unbind('click').click(function () {
        var oldpassword = $('#txtOldPassword').val();
        var newpassword = $('#txtNewPassword').val();
        var confirmpassword = $('#txtConfirmPassword').val();

        if (newpassword == confirmpassword) {
            changePassword(function (result) {
                if (result == 'Success') {
                    $('#changePassword').modal('hide');
                    showMessage('Password successfully changed!');
                }
                else showMessage(result);
                return false;
            }, accountData.ID, oldpassword, newpassword);
        }
        else showMessage('Passwords do not match.');
    });

    $('#cmdUpdateProfile').unbind('click').click(function () {
        var firstname = $('#txtFirstName').val();
        var lastname = $('#txtLastName').val();

        updateProfile(function (result) {
            if (result == 'Success') {
                location.reload();
            }
            else showMessage(result);
        }, accountData.ID, firstname, lastname);
    });
}