﻿var divHide;
var btnEdit;
$(document).ready(function () {
    getI9Items(function (result) {
        var containers = ["collapsePanelOneInner", "collapsePanelTwoInner", "collapsePanelThreeInner", "collapsePanelFourInner", "collapsePanelFiveInner"];

        for (var i = 0; i < containers.length; i++) {
            var i9Items = $.grep(result, function (e) { return e.HtmlContainer == containers[i]; });
            for (var j = 0; j < i9Items.length; j++) {
                var str = '<div id="' + i9Items[j].ID + '">';
                str += i9Items[j].OrderInContainer + ') ' + i9Items[j].Question;
                str += '<br /><br />';
                if (i9Items[j].AnswerType == "Radio") {
                    var checked = "checked";
                    for (var k = 0; k < i9Items[j].CustomAnswers.length; k++) {
                        if (i9Items[j].Answer == i9Items[j].CustomAnswers[k].ID) checked = "checked";
                        str += '<input type="radio" name="radioAnswers' + j + '" data-answer=' + i9Items[j].CustomAnswers[k].ID + ' ' + checked + '/>\n' + i9Items[j].CustomAnswers[k].Answer + '<br />';
                        checked = "";
                    }
                    str += '<br/><button class="btn btn-info" data-submitradio="true" data-radiogroup="radioAnswers' + j + '">Submit</button>\n';
                }
                else {
                    var btnClass = ["btn-info", "btn-info", "btn-info", "btn-info"]
                    if (i9Items[j].Answer > 0) btnClass[i9Items[j].Answer - 1] = "btn-success";
                    str += '<button class="btn ' + btnClass[0] + '" data-submit="true" data-answer=' + buttonOptions[0][0].ID + '>' + buttonOptions[0][0].Text + '</button>\n';
                    str += '<button class="btn ' + btnClass[1] + '" data-submit="true" data-answer=' + buttonOptions[0][1].ID + '>' + buttonOptions[0][1].Text + '</button>\n';
                    str += '<button class="btn ' + btnClass[2] + '" data-submit="true" data-answer=' + buttonOptions[0][2].ID + '>' + buttonOptions[0][2].Text + '</button>\n';
                    str += '<button class="btn ' + btnClass[3] + '" data-submit="true" data-answer=' + buttonOptions[0][3].ID + '>' + buttonOptions[0][3].Text + '</button>';
                }
                str += '<hr />';
                str += '</div>';

                $('#' + containers[i]).append(str);
            }
        }
        $('button[data-submit="true"]').unbind('click').click(function () {
            btnEdit = $(this);
            divHide = $(this).closest('div');
            postI9Submit(function (result) {
                if (result == 'Success') {
                    if ($(location).attr('pathname').indexOf('Edit') != -1) {
                        btnEdit.siblings(".btn-success").removeClass("btn-success").addClass("btn-info");
                        btnEdit.removeClass("btn-info").addClass("btn-success");
                    }
                    else divHide.hide();
                    showMessage('The answer successfully submitted')
                }
                else showMessage(result);
            }, divHide.attr('id'), $(this).attr('data-answer'));
        });

        $('button[data-submitradio="true"]').unbind('click').click(function () {
            divHide = $(this).closest('div');
            postI9Submit(function (result) {
                if (result == 'Success') {
                    if ($(location).attr('pathname').indexOf('Edit') != -1) {
                    }
                    else divHide.hide();
                    showMessage('The answer successfully submitted');
                }
                else showMessage(result);
            }, divHide.attr('id'), $('input[name=' + $(this).attr('data-radiogroup') + ']:checked').attr('data-answer'));
        });
    });
});
