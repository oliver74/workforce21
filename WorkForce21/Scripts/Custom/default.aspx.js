﻿$(document).ready(function () {
    //    registerpnl();
    $('#rootwizard').bootstrapWizard({
        onTabClick: function (tab, navigation, index) {
            return false;
        }
    });
    getQuestionnaire(function (result) {
        if (result.length == 0) {
            $('#quesresult').hide();
            doPost = true;
        }
        else {
            while (step != -1) {
                var action = false;
                switch (step) {
                    case 1:
                        if (result[0].EE50OrMore) action = true;
                        break;
                    case 2:
                        if (result[0].EE20_49) action = true;
                        break;
                    case 3:
                        if (result[0].EE15_19) action = true;
                        break;
                    case 4:
                        if (result[0].OfferHealthInsurance) action = true;
                        break;
                    case 5:
                        if (result[0].Contractors1099) action = true;
                        break;
                }
                if (action) onClickYes();
                else onClickNo();
            }
        }
    }, clientid[0]);
});

var step = 1;
var steps = [1];
var answers = [];
var doPost = false;
$('button[data-btnyes="true"]').unbind('click').click(function () {
    onClickYes();
    return false;
});

$('button[data-btnno="true"]').unbind('click').click(function () {
    onClickNo();
    return false;
});

$('button[data-moreinfo="true"]').unbind('click').click(function () {
    var str = '';
    switch ($(this).attr('data-item')) {
        case 'I9':
            str = '<p><b><i>Scope of Audit:</i></b> The I-9 Audit consists of 45 questions (actual number varies based on provided answers) and we estimate will take you 15-20 minutes to complete.  The audit provides an assessment of the I-9 completion process, document completion, storage, and retention, document verification and re-verification requirements, and E-verify where applicable.</p>';
            str += '<p><b><i>About form I-9:</i></b> Form I-9 is used for verifying the identity and employment authorization of individuals hired for employment in the United States. All U.S. employers must ensure proper completion of Form I-9 for each individual they hire for employment in the United States. This includes citizens and noncitizens. Both employees and employers (or authorized representatives of the employer) must complete the form. On the form, an employee must attest to his or her employment authorization. The employee must also present his or her employer with acceptable documents evidenceing identity and employment authorization. The employer must examine the employment eligibility and identity document(s) an employee presents to determine whether the document(s) reasonably appear to be genuine and to relate to the employee and record the document information on the Form I-9. The list of acceptable documents can be found on the last page of the form. Employers must retain Form I-9 for a designated period and make it available for inspection by authorized government officers. NOTE: State agencies may use Form I-9. Also, some agricultural recruiters and referrers for a fee may be required to use Form I-9.</p>';
            $('#moreInfoDlg .modal-title').html('Form I-9');
            break;
        case 'ADA':
            str = '<p><b><i>Scope of Audit:</i></b> The Americans with Disabilities Act audit consists of 12 questions and several scenarios. We estimate this will take you approximately 20-25 minutes to complete.  The audit provides an assessment of use of medical examinations and questionnaires, internal policies on disability accommodations, employment candidates, and your overall ADA process. Interaction with FMLA is also assessed where applicable. </p>';
            str += '<p><b><i>About the Americans with Disabilities Act:</i></b> Employers with 15 or more employees are prohibited from discriminating against people with disabilities by Title I of the Americans with Disabilities Act (ADA). In general, the employment provisions of the ADA require:</p>';
            str += '<ul><li>equal opportunity in selecting, testing, and hiring qualified applicants with disabilities;</li>';
            str += '<li>job accommodation for applicants and workers with disabilities when such accommodations would not impose "undue hardship;" and</li>';
            str += '<li>equal opportunity in promotion and benefits.</li></ul>';
            str += '<p>To be protected by the ADA, one must have a disability, which is defined by the ADA as a physical or mental impairment that substantially limits one or more major life activities, a person who has a history or record of such an impairment, or a person who is perceived by others as having such an impairment. The ADA does not specifically name all of the impairments that are covered.</p>';
            str += "<p>The Department of Labor's Office of Disability Employment Policy (ODEP)provides information on the ADA, but the law’s enforcement for Title I (employment) of the ADA  is through the US Equal Employment Opportunity Commission.</p>";
            $('#moreInfoDlg .modal-title').html('Americans with Disabilities Act');
            break;
        case 'FLSA':
            str = '<p><b><i>Scope of Audit:</i></b> The Fair Labor Standards Act audit consists of 32 questions and we estimate will take you approximately 20 minutes to complete.  The audit provides an assessment of the assigned job classifications (exempt and non-exempt) for your employees, wages and overtime, and breaks and off-the-clock work. </p>';
            str += "<p><b><i>About the Fair Labor Standards Act:</i></b> The Fair Labor Standards Act (FLSA), which prescribes standards for the basic minimum wage and overtime pay, affects most private and public employment. It requires employers to pay covered employees who are not otherwise exempt at least the federal minimum wage and overtime pay of one-and-one-half-times the regular rate of pay. For nonagricultural operations, it restricts the hours that children under age 16 can work and forbids the employment of children under age 18 in certain jobs deemed too dangerous. For agricultural operations, it prohibits the employment of children under age 16 during school hours and in certain jobs deemed too dangerous. The Act is administered by the Employment Standards Administration's Wage and Hour Division within the U.S. Department of Labor.</p>";
            str += '<p>Employers may be assessed civil money penalties of up to $1,100 for each willful or repeated violation of the minimum wage or overtime pay provisions of the law and up to $11,000 for each employee who is the subject of a violation of the Act’s child labor provisions. In addition, a civil money penalty of up to $50,000 may be assessed for each child labor violation that causes the death or serious injury of any minor employee, and such assessments may be doubled, up to $100,000, when the violations are determined to be willful or repeated. The law also prohibits discriminating against or discharging workers who file a complaint or participate in any proceeding under the Act.</p>';
            $('#moreInfoDlg .modal-title').html('The Fair Labor Standards Act');
            break;
        case 'ADEA':
            str = '<p>Age discrimination involves treating someone (an applicant or employee) less favorably because of their age.</p>';
            str += '<p>The Age Discrimination in Employment Act (ADEA) prohibits an employer from refusing to hire, firing, or otherwise discriminating against an employee age 40 or older, solely on the basis of age. Thus, an employer can’t deny an employee pay or fringe benefits when the only justification is age. Nor may an employer classify employees into groups on the basis of age in a way that unfairly deprives workers of employment opportunities. For example, an employer may not relegate all older workers to a particular level of employment within a company and then decline to promote them.</p>';
            str += '<p>In almost all cases, employers cannot require employees to retire or otherwise change job positions based on age provided the employee is capable of performing their essential duties.  The Age Discrimination in Employment Act carves out a compulsory retirement exception for “bona fide executives” or “high policymakers” – i.e.,an employer may impose compulsory retirement on any employee age 65 or older who is either a bona fide executive or high policymaker and who is entitled to receive a non-forfeitable annual retirement benefit.</p>';
            str += '<p>The Age Discrimination in Employment Act defines “employer” to include every individual, partnership, association, labor organization, corporation, business trust, legal representative, or organized group of persons who</p>';
            str += '<ul><li>is engaged in an industry affecting commerce (most every industry will affect commerce within the meaning of the ADEA); and</li>';
            str += '<li>has 20 or more employees for each working day in each of 20 or more calendar weeks in the current or preceding calendar year.</li></ul>';
            $('#moreInfoDlg .modal-title').html('Age Discrimination Employment Act');
            break;
        case 'FMLA':
            str = '<p>The FMLA applies to employers that meet certain criteria. A covered employer is a: </p>';
            str += '<ul><li>Private-sector employer, with 50 or more employees in 20 or more workweeks in the current or preceding calendar year, including a joint employer or successor in interest to a covered employer;</li>';
            str += '<li>Public agency, including a local, state, or Federal government agency, regardless of the number of employees it employs; or</li>';
            str += '<li>Public or private elementary or secondary school, regardless of the number of employees it employs.</li></ul>';
            str += '<p>The FMLA entitles eligible employees of covered employers to take unpaid, job-protected leave for specified family and medical reasons with continuation of group health insurance coverage under the same terms and conditions as if the employee had not taken leave. Eligible employees are entitled to:</p>';
            str += '<p>Twelve workweeks of leave in a 12-month period for:</p>';
            str += '<ul><li>the birth of a child and to care for the newborn child within one year of birth;</li>';
            str += '<li>the placement with the employee of a child for adoption or foster care and to care for the newly placed child within one year of placement;</li>';
            str += '<li>to care for the employee’s spouse, child, or parent who has a serious health condition;</li>';
            str += '<li>a serious health condition that makes the employee unable to perform the essential functions of his or her job;</li>';
            str += '<li>any qualifying exigency arising out of the fact that the employee’s spouse, son, daughter, or parent is a covered military member on “covered active duty;” or</li>';
            str += '<li>Twenty-six workweeks of leave during a single 12-month period to care for a covered service member with a serious injury or illness if the eligible employee is the service member’s spouse, son, daughter, parent, or next of kin (military caregiver leave).</li></ul>';
            $('#moreInfoDlg .modal-title').html('The Family and Medical Leave Act');
            break;
        case 'HPS':
            str = '<p>Employers who choose to provide their employees with access to health care insurance are required to comply with various federal (and in many cases state) requirements. Those legislative requirements include:</p>';
            str += '<p><b><i>ERISA:</i></b> The Employee Retirement Income Security Act of 1974 (ERISA) is a federal law that sets minimum standards for most voluntarily established pension and health plans in private industry to provide protection for individuals in these plans. In general, ERISA does not cover group health plans established or maintained by governmental entities, churches for their employees, or plans which are maintained solely to comply with applicable workers compensation, unemployment, or disability laws. ERISA requires plans to provide participants with plan information including important information about plan features and funding; provides fiduciary responsibilities for those who manage and control plan assets; requires plans to establish a grievance and appeals process for participants to get benefits from their plans; and gives participants the right to sue for benefits and breaches of fiduciary duty.</p>';
            str += '<p>ERISA includes the Consolidated Omnibus Budget Reconciliation Act (COBRA) that provides some workers and their families with the right to continue their health coverage for a limited time after certain events, such as the loss of a job.</p>';
            str += '<p><b><i>The Affordable Care Act (ACA):</i></b> The ACA is part of ERISA by incorporation. Among the consequences of that incorporation is that the ACA violations may also be ERISA violations, resulting in claims under ERISA. PPACA does not contain provisions through which efforts at PPACA compliance provide immunity from other federal laws.</p>';
            str += '<p><b><i>The Internal Revenue Service (IRS):</i></b> The IRS provides, in certain cases, tax advantages to employers sponsoring health care plans and employees electing to participate in such plans.  As such there are important requirements established by IRS codes section 105 and section 125 that employers must comply with.  Employers failing to comply with such requirements are subject to penalties and fines including retroactive application to the dates of first violation.</p>';
            $('#moreInfoDlg .modal-title').html('Health Plan Sponsorship');
            break;
    }
    $('#moreInfoDlg .modal-body').html(str);
    $("#moreInfoDlg").modal('show');
    return false;
});

$('#btnPrevious').unbind('click').click(function () {
    if (steps.length > 1) {
        answers.pop();
        steps.pop();
        step = steps[steps.length - 1];
        nextQuestion(step);
        if (steps.length == 1) $('#btnPrevious').parent().addClass('disabled');
    }
});

$('#btnResetQuest').unbind('click').click(function () {
    step = 1;
    $('.tab-pane div').removeClass('active');
    $('.tab-pane#tab' + step).addClass('active');
    $('#btnPrevious').parent().addClass('disabled');
    $('td[data-tdreq="true"]').html('No');
    $('#rootwizard').show();
    $('#quesresult').hide();
    steps = [1];
    answers = [];
    doPost = true;
});

$('#btnPurchase').unbind('click').click(function () {
    var selected = '';
    $('input[data-purchase="true"]:checked').each(function () {
        selected += $(this).attr('data-item') + ',';
    });

    if (selected != '') {
        postPurchaseSubmit(function (result) {
            if (result == 'Success') showMessage("Purchase has been successfully initiated. You'll get an email when the purchase completes.");
            else showMessage(result);
        }, selected.substring(0, selected.length - 1));
    }
});

function nextQuestion(step) {
    if (step > 0) {
        $('.tab-pane div').removeClass('active');
        $('.tab-pane#tab' + step).addClass('active');
        $('#btnPrevious').parent().removeClass('disabled');
    }
    else {
        $('#rootwizard').hide();
        $('#quesresult').show();
        populateTable();
    }
}

function populateTable() {
    for (var i = 0; i < answers.length; i++) {
        switch (steps[i]) {
            case 1:
                if (answers[i] == 'yes') {
                    $('#tdReq1').html('Yes');
                    $('#tdReq2').html('Yes');
                    $('#tdReq3').html('Yes');
                    $('#tdReq4').html('Yes');
                    $('#tdReq5').html('Yes');
                    if ($.inArray('I9', purchaseditems[0]) == -1) $('input[data-item="I9"]').prop('disabled', false);
                    if ($.inArray('ADA', purchaseditems[0]) == -1) $('input[data-item="ADA"]').prop('disabled', false);
                    if ($.inArray('FLSA', purchaseditems[0]) == -1) $('input[data-item="FLSA"]').prop('disabled', false);
                    if ($.inArray('ADEA', purchaseditems[0]) == -1) $('input[data-item="ADEA"]').prop('disabled', false);
                    if ($.inArray('FMLA', purchaseditems[0]) == -1) $('input[data-item="FMLA"]').prop('disabled', false);
                }
                break;
            case 2:
                if (answers[i] == 'yes') {
                    $('#tdReq1').html('Yes');
                    $('#tdReq2').html('Yes');
                    $('#tdReq3').html('Yes');
                    $('#tdReq4').html('Yes');
                    if ($.inArray('I9', purchaseditems[0]) == -1) $('input[data-item="I9"]').prop('disabled', false);
                    if ($.inArray('ADA', purchaseditems[0]) == -1) $('input[data-item="ADA"]').prop('disabled', false);
                    if ($.inArray('FLSA', purchaseditems[0]) == -1) $('input[data-item="FLSA"]').prop('disabled', false);
                    if ($.inArray('ADEA', purchaseditems[0]) == -1) $('input[data-item="ADEA"]').prop('disabled', false);
                }
                break;
            case 3:
                if (answers[i] == 'yes') {
                    $('#tdReq1').html('Yes');
                    $('#tdReq2').html('Yes');
                    $('#tdReq3').html('Yes');
                    if ($.inArray('I9', purchaseditems[0]) == -1) $('input[data-item="I9"]').prop('disabled', false);
                    if ($.inArray('ADA', purchaseditems[0]) == -1) $('input[data-item="ADA"]').prop('disabled', false);
                    if ($.inArray('FLSA', purchaseditems[0]) == -1) $('input[data-item="FLSA"]').prop('disabled', false);
                }
                else {
                    $('#tdReq1').html('Yes');
                    if ($.inArray('I9', purchaseditems[0]) == -1) $('input[data-item="I9"]').prop('disabled', false);
                }
                break;
            case 4:
                if (answers[i] == 'yes') {
                    $('#tdReq6').html('Yes');
                    if ($.inArray('HPS', purchaseditems[0]) == -1) $('input[data-item="HPS"]').prop('disabled', false);
                }
                break;
            case 5:
                if (answers[i] == 'yes') {
                    $('#tdReq7').html('Yes');
                    if ($.inArray('CR', purchaseditems[0]) == -1) $('input[data-item="CR"]').prop('disabled', false);
                }
                break;
        }
    }
    for (var i = 0; i < expiringitems[0].length; i++) $('#tdExp' + (i + 1)).html(expiringitems[0][i]);

    if (doPost) {
        postQuestionnaire(function (result) {
            if (result == 'Success') {
                showMessage('The questionnaire successfully completed')
            }
            else showMessage(result);
        }, clientid[0], createQuestionnaireObject());
        doPost = false;
    }
}

function createQuestionnaireObject() {
    return {
        "EE50OrMore": steps.indexOf(1) < 0 ? false : convertStringToBool(answers[steps.indexOf(1)]),
        "EE20_49": steps.indexOf(2) < 0 ? false : convertStringToBool(answers[steps.indexOf(2)]),
        "EE15_19": steps.indexOf(3) < 0 ? false : convertStringToBool(answers[steps.indexOf(3)]),
        "OfferHealthInsurance": steps.indexOf(4) < 0 ? false : convertStringToBool(answers[steps.indexOf(4)]),
        "Contractors1099": steps.indexOf(5) < 0 ? false : convertStringToBool(answers[steps.indexOf(5)])
    };
}

function convertStringToBool(string) {
    return string == 'yes' ? true : false;
}

function onClickYes() {
    switch (step) {
        case 1:
        case 2:
        case 3:
            step = 4;
            break;
        case 4:
            step = 5;
            break;
        case 5:
        default:
            step = -1;
            break;
    }
    steps.push(step);
    answers.push('yes');
    nextQuestion(step);
}

function onClickNo() {
    switch (step) {
        case 1:
            step = 2;
            break;
        case 2:
            step = 3;
            break;
        case 3:
            step = 4;
            break;
        case 4:
            step = 5;
            break;
        case 5:
        default:
            step = -1;
            break;
    }
    steps.push(step);
    answers.push('no');
    nextQuestion(step);
}
