﻿var divHide;
var btnEdit;
$(document).ready(function () {
    getFlsaItems(function (result) {
        var containers = ["collapsePanelOneInner", "collapsePanelTwoInner", "collapsePanelThreeInner"];

        for (var i = 0; i < containers.length; i++) {
            var flsaItems = $.grep(result, function (e) { return e.HtmlContainer == containers[i]; });
            for (var j = 0; j < flsaItems.length; j++) {
                var str = '<div id="' + flsaItems[j].ID + '">';
                str += flsaItems[j].OrderInContainer + ') ' + flsaItems[j].Question;
                str += '<br /><br />';
                if (flsaItems[j].AnswerType == "Radio") {
                    var checked = "checked";
                    for (var k = 0; k < flsaItems[j].CustomAnswers.length; k++) {
                        str += '<input type="radio" name="radioAnswers' + j + '" data-answer=' + flsaItems[j].CustomAnswers[k].ID + ' ' + checked + '/>\n' + flsaItems[j].CustomAnswers[k].Answer + '<br />';
                        checked = "";
                    }
                }
                else {
                    var btnClass = ["btn-info", "btn-info", "btn-info", "btn-info"]
                    if (flsaItems[j].Answer > 0) btnClass[flsaItems[j].Answer - 1] = "btn-success";
                    str += '<button class="btn ' + btnClass[0] + '" data-submit="true" data-answer=' + buttonOptions[0][0].ID + '>' + buttonOptions[0][0].Text + '</button>\n';
                    str += '<button class="btn ' + btnClass[1] + '" data-submit="true" data-answer=' + buttonOptions[0][1].ID + '>' + buttonOptions[0][1].Text + '</button>\n';
                    str += '<button class="btn ' + btnClass[2] + '" data-submit="true" data-answer=' + buttonOptions[0][2].ID + '>' + buttonOptions[0][2].Text + '</button>\n';
                    str += '<button class="btn ' + btnClass[3] + '" data-submit="true" data-answer=' + buttonOptions[0][3].ID + '>' + buttonOptions[0][3].Text + '</button>';
                }
                str += '<hr />';
                str += '</div>';

                $('#' + containers[i]).append(str);
            }
        }
        $('button[data-submit="true"]').unbind('click').click(function () {
            btnEdit = $(this);
            divHide = $(this).closest('div');
            postFlsaSubmit(function (result) {
                if (result == 'Success') {
                    if ($(location).attr('pathname').indexOf('Edit') != -1) {
                        btnEdit.siblings(".btn-success").removeClass("btn-success").addClass("btn-info");
                        btnEdit.removeClass("btn-info").addClass("btn-success");
                    }
                    else divHide.hide();
                    showMessage('The answer successfully submitted')
                }
                else showMessage(result);
            }, divHide.attr('id'), $(this).attr('data-answer'));
        });
    });
});
