﻿var divHide;
var btnEdit;
$(document).ready(function () {
    getHPSponsorItems(function (result) {
        var containers = ["collapsePanelOneInner"];

        for (var i = 0; i < containers.length; i++) {
            var items = $.grep(result, function (e) { return e.HtmlContainer == containers[i]; });
            for (var j = 0; j < items.length; j++) {
                var str = '<div id="' + items[j].ID + '">';
                str += items[j].OrderInContainer + ') ' + items[j].Question;
                str += '<br /><br />';
                if (items[j].AnswerType == "Radio") {
                    var checked = "checked";
                    for (var k = 0; k < items[j].CustomAnswers.length; k++) {
                        str += '<input type="radio" name="radioAnswers' + j + '" data-answer=' + items[j].CustomAnswers[k].ID + ' ' + checked + '/>\n' + items[j].CustomAnswers[k].Answer + '<br />';
                        checked = "";
                    }
                }
                else {
                    var btnClass = ["btn-info", "btn-info", "btn-info", "btn-info"]
                    if (items[j].Answer > 0) btnClass[items[j].Answer - 1] = "btn-success";
                    str += '<button class="btn ' + btnClass[0] + '" data-submit="true" data-answer=' + buttonOptions[0][0].ID + '>' + buttonOptions[0][0].Text + '</button>\n';
                    str += '<button class="btn ' + btnClass[1] + '" data-submit="true" data-answer=' + buttonOptions[0][1].ID + '>' + buttonOptions[0][1].Text + '</button>\n';
                    str += '<button class="btn ' + btnClass[2] + '" data-submit="true" data-answer=' + buttonOptions[0][2].ID + '>' + buttonOptions[0][2].Text + '</button>\n';
                    str += '<button class="btn ' + btnClass[3] + '" data-submit="true" data-answer=' + buttonOptions[0][3].ID + '>' + buttonOptions[0][3].Text + '</button>';
                }
                str += '<hr />';
                str += '</div>';

                $('#' + containers[i]).append(str);
            }
        }
        $('button[data-submit="true"]').unbind('click').click(function () {
            btnEdit = $(this);
            divHide = $(this).closest('div');
            postHPSponsorSubmit(function (result) {
                if (result == 'Success') {
                    if ($(location).attr('pathname').indexOf('Edit') != -1) {
                        btnEdit.siblings(".btn-success").removeClass("btn-success").addClass("btn-info");
                        btnEdit.removeClass("btn-info").addClass("btn-success");
                    }
                    else divHide.hide();
                    showMessage('The answer successfully submitted')
                }
                else showMessage(result);
            }, divHide.attr('id'), $(this).attr('data-answer'));
        });
    });
});
