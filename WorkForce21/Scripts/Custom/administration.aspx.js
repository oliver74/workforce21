﻿$(document).ready(function () {
    //$('#mastertitle').text('Advisors Administration');
    populateClients();
});

function populateClients() {
    getClients(function (clients) {
        $('#tblClients tbody').html('');
        for (var i = 0; i < clients.length; i++) {
            str = '<tr id="' + clients[i].ID + '" data-clientname="' + clients[i].Company + '">'
            str += '<td class="centeralign">'
            str += '<div class="btn-group">'
            str += '<button data-toggle="dropdown" class="btn dropdown-toggle">Action <span class="caret"></span></button>'
            str += '<ul class="dropdown-menu">'
            str += '<li><a href="#" data-editmodal="true">Edit</a></li>'
            str += '<li><a href="#" data-deletemodal="true">Delete</a></li>'
            str += '</ul>'
            str += '</div>'
            str += '</td>'
            str += '<td>' + clients[i].Company + '</td>'
            str += '<td>' + normalizeString(clients[i].Address) + '</td>'
            str += '<td>' + normalizeString(clients[i].City) + '</td>'
            str += '<td>' + normalizeString(clients[i].Zip) + '</td>'
            str += '<td>' + clients[i].FirstName + ' ' + clients[i].LastName + '</td>'
            str += '<td>' + normalizeString(clients[i].Phone) + '</td>'
            str += '<td>' + clients[i].Email + '</td>'
            str += '<td>' + isAdmin(clients[i].Admin) + '</td>'
            str += '</tr>'
            $('#tblClients tbody').append(str);
            $('tr[id="' + clients[i].ID + '"]').data('data', clients[i]);
        }

        $('a[data-editmodal="true"]').unbind('click').click(function () {
            $tableRow = $(this).closest('tr');
            $('#hfClientAction').val('Edit');
            $('#hfClientId').val($tableRow.attr('id'));
            var client = $tableRow.data('data');
            $('#txtCompany').val(client.Company);
            $('#txtAddress').val(client.Address);
            $('#txtCity').val(client.City);
            $('#txtZip').val(client.Zip);
            $('#txtFirstName').val(client.FirstName);
            $('#txtLastName').val(client.LastName);
            $('#txtPhone').val(client.Phone);
            $('#txtEmail').val(client.Email);
            $('#cbIsAdmin').prop('checked', client.Admin);
            $('#addClientDlg').modal('show');
            return false;
        });

        $('a[data-deletemodal="true"]').unbind('click').click(function () {
            $tableRow = $(this).closest('tr');
            $('#hfClientAction').val('Delete');
            $('#hfClientId').val($tableRow.attr('id'));
            $('#lblActionMessage').html('Are you sure you want to delete the client ' + $tableRow.attr('data-clientname') + '?');
            $('#confirmActionDlg').modal('show');
            return false;
        });
    });
}

$('button[data-addclient="true"]').unbind('click').click(function () {
    $('#addClientDlg input[type=text]').each(function () {
        $(this).val('');
        $(this).css({
            "border": "",
            "background": ""
        });
    });
    $('#cbIsAdmin').prop('checked', false);
    $('#hfClientAction').val('Add');
    $('#addClientDlg').modal('show');
    return false;
});

$('#cmdSaveClient').unbind('click').click(function () {
    if (!isInputValid()) {
        showMessage('Please populate all client mandatory fields');
        return;
    }

    switch ($('#hfClientAction').val()) {
        case "Add":
            postAddClientSave(function (result) {
                if (result == 'Success') {
                    showMessage('The client successfully added')
                    populateClients();
                }
                else showMessage(result);
            }, createClientObject());
        break;

        case "Edit":
            putEditClientSave(function (result) {
                if (result == 'Success') {
                    showMessage('The client successfully modified')
                    populateClients();
                }
                else showMessage(result);
            }, createClientObject(), $('#hfClientId').val());
        break;
    }
    $('#addClientDlg').modal('hide');
});

$('#cmdActionYes').unbind('click').click(function () {
    switch ($('#hfClientAction').val()) {
        case "Delete":
            postClientDelete(function (result) {
                //checking to see what data came back and displaying the message
                if (result == 'Success') {
                    showMessage('The client successfully deleted!');
                    populateClients();
                }
                else showMessage(result);
            }, $('#hfClientId').val());
            break;

        default:
            break;
    }
    $('#confirmActionDlg').modal('hide');
});

function createClientObject() {
    return {
        "Company": $('#txtCompany').val(),
        "Address": $('#txtAddress').val(),
        "City": $('#txtCity').val(),
        "Zip": $('#txtZip').val(),
        "FirstName": $('#txtFirstName').val(),
        "LastName": $('#txtLastName').val(),
        "Phone": $('#txtPhone').val(),
        "Email": $('#txtEmail').val(),
        "Admin": $('#cbIsAdmin').prop('checked')
    };
}

function isInputValid() {
    var isValid = true;
    $('#addClientDlg input[data-addmandatory="true"]').each(function () {
        if ($.trim($(this).val()) == '') {
            isValid = false;
            $(this).css({
                "border": "1px solid red",
                "background": "#FFCECE"
            });
        }
        else {
            $(this).css({
                "border": "",
                "background": ""
            });
        }
    });
    return isValid;
}

function isAdmin(admin) {
    return admin == true ? 'Yes' : 'No';
}

function normalizeString(value) {
    return value === null ? "" : value;
}