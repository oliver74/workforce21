﻿var divHide;
var btnEdit;
$(document).ready(function () {
    getAdaItems(function (result) {
        var containers = ["collapsePanelOneInner"];

        for (var i = 0; i < containers.length; i++) {
            var adaItems = $.grep(result, function (e) { return e.HtmlContainer == containers[i]; });
            for (var j = 0; j < adaItems.length; j++) {
                var str = '<div id="' + adaItems[j].ID + '">';
                str += adaItems[j].OrderInContainer + ') ' + adaItems[j].Question;
                str += '<br /><br />';
                if (adaItems[j].AnswerType == "Radio") {
                    var checked = "checked";
                    for (var k = 0; k < adaItems[j].CustomAnswers.length; k++) {
                        if (adaItems[j].Answer == adaItems[j].CustomAnswers[k].ID) checked = "checked";
                        str += '<input type="radio" name="radioAnswers' + j + '" data-answer=' + adaItems[j].CustomAnswers[k].ID + ' ' + checked + '/>\n' + adaItems[j].CustomAnswers[k].Answer + '<br />';
                        checked = "";
                    }
                    str += '<br/><button class="btn btn-info" data-submitradio="true" data-radiogroup="radioAnswers' + j +'">Submit</button>\n';
                }
                else {
                    var btnClass = ["btn-info", "btn-info", "btn-info", "btn-info"]
                    if (adaItems[j].Answer > 0) btnClass[adaItems[j].Answer - 1] = "btn-success";
                    str += '<button class="btn ' + btnClass[0] + '" data-submit="true" data-answer=' + buttonOptions[0][0].ID + '>' + buttonOptions[0][0].Text + '</button>\n';
                    str += '<button class="btn ' + btnClass[1] + '" data-submit="true" data-answer=' + buttonOptions[0][1].ID + '>' + buttonOptions[0][1].Text + '</button>\n';
                    str += '<button class="btn ' + btnClass[2] + '" data-submit="true" data-answer=' + buttonOptions[0][2].ID + '>' + buttonOptions[0][2].Text + '</button>\n';
                    str += '<button class="btn ' + btnClass[3] + '" data-submit="true" data-answer=' + buttonOptions[0][3].ID + '>' + buttonOptions[0][3].Text + '</button>';
                }
                str += '<hr />';
                str += '</div>';

                $('#' + containers[i]).append(str);
            }
        }
        $('button[data-submit="true"]').unbind('click').click(function () {
            btnEdit = $(this);
            divHide = $(this).closest('div');
            postAdaSubmit(function (result) {
                if (result == 'Success') {
                    if ($(location).attr('pathname').indexOf('Edit') != -1) {
                        btnEdit.siblings(".btn-success").removeClass("btn-success").addClass("btn-info");
                        btnEdit.removeClass("btn-info").addClass("btn-success");
                    }
                    else divHide.hide();
                    showMessage('The answer successfully submitted');
                }
                else showMessage(result);
            }, divHide.attr('id'), $(this).attr('data-answer'));
        });

        $('button[data-submitradio="true"]').unbind('click').click(function () {
            divHide = $(this).closest('div');
            postAdaSubmit(function (result) {
                if (result == 'Success') {
                    if ($(location).attr('pathname').indexOf('Edit') != -1) {
                    }
                    else divHide.hide();
                    showMessage('The answer successfully submitted');
                }
                else showMessage(result);
            }, divHide.attr('id'), $('input[name=' + $(this).attr('data-radiogroup') + ']:checked').attr('data-answer'));
        });
    });
});
