﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Web;
using System.Web.Http;
using WorkForce21.DAL;
using WorkForce21.Models;
using WorkForce21.Utilities;
using WotkForce21.Utilities;

namespace WorkForce21.Controllers
{
    [RoutePrefix("administration")]
    public class AdministrationController : ApiController
    {
        private WFContext db = new WFContext();

        [HttpGet]
        [Route("clients")]
        [WFAuthorize(Roles = "Admin")]
        public IEnumerable<Client> Get()
        {
            return db.Clients.Where(e => e.Active);
        }

        [HttpPost]
        [Route("clients")]
        [WFAuthorize(Roles = "Admin")]
        public HttpResponseMessage Post(Client pClient)
        {
            string password = CreateRandomPassword(8);
            pClient.Active = true;
            pClient.Password = PasswordHash.CreateHash(password);
            db.Clients.Add(pClient);
            SaveChanges(pClient);
            Email.SendPassword(pClient.Email, password);
            return Request.CreateResponse<string>(HttpStatusCode.OK, "Success");
        }

        [HttpPut]
        [Route("clients/{id:int}")]
        [WFAuthorize(Roles = "Admin")]
        public HttpResponseMessage Put(int id, Client pClient)
        {
            Client client = db.Clients.Find(id);
            if (client == null)
            {
                throw new HttpResponseException(Request.CreateResponse<string>(HttpStatusCode.NotFound, string.Format("The client not found.")));
            }
            client.Company = pClient.Company;
            client.Address = pClient.Address;
            client.City = pClient.City;
            client.Zip = pClient.Zip;
            client.FirstName = pClient.FirstName;
            client.LastName = pClient.LastName;
            client.Phone = pClient.Phone;
            client.Email = pClient.Email;
            client.Admin = pClient.Admin;
            SaveChanges(client);
            return Request.CreateResponse<string>(HttpStatusCode.OK, "Success");
        }

        [HttpDelete]
        [Route("clients/{id:int}")]
        [WFAuthorize(Roles = "Admin")]
        public HttpResponseMessage Delete(int id)
        {
            Client client = db.Clients.Find(id);
            if (client == null)
            {
                throw new HttpResponseException(Request.CreateResponse<string>(HttpStatusCode.NotFound, string.Format("The client not found.")));
            }
            client.Active = false;
            SaveChanges(client);
            return Request.CreateResponse<string>(HttpStatusCode.OK, "Success");
        }

        [HttpPost]
        [Route("clients/resetpassword")]
        [AllowAnonymous]
        public HttpResponseMessage Post([FromBody] string email)
        {
            string password = CreateRandomPassword(8);
            Client client = db.Clients.FirstOrDefault(e => e.Email.Equals(email));
            if (client == null)
            {
                throw new HttpResponseException(Request.CreateResponse<string>(HttpStatusCode.NotFound,
                    string.Format("The client with the email {0} not found.", email)));
            }
            client.Password = PasswordHash.CreateHash(password);
            Email.SendPassword(email, password);
            return Request.CreateResponse<string>(HttpStatusCode.OK, "Success");
        }

        [HttpPost]
        [Route("clients/registeruser")]
        [AllowAnonymous]
        public HttpResponseMessage PostRegister([FromBody] string email)
        {
            string password = CreateRandomPassword(8);
            Client client = db.Clients.FirstOrDefault(e => e.Email.Equals(email));
            if (client != null)
            {
                throw new HttpResponseException(Request.CreateResponse<string>(HttpStatusCode.NotFound,
                    string.Format("The client with the email {0} has already been registered.", email)));
            }
            client = new Client { Email = email, Password = PasswordHash.CreateHash(password) };
            db.Clients.Add(client);
            db.SaveChanges();
            Email.SendPassword(email, password);
            return Request.CreateResponse<string>(HttpStatusCode.OK, "Success");
        }

        [HttpPost]
        [Route("clients/updateprofile")]
        [WFAuthorize(Roles = "Standard")]
        public HttpResponseMessage PostUpdateProfile(Client pClient)
        {
            Client client = db.Clients.Find(pClient.ID);
            if (client == null)
            {
                throw new HttpResponseException(Request.CreateResponse<string>(HttpStatusCode.NotFound,
                    string.Format("The client with the ID {0} not found.", pClient.ID)));
            }
            client.FirstName = pClient.FirstName;
            client.LastName = pClient.LastName;
            SaveChanges(client);
            return Request.CreateResponse<string>(HttpStatusCode.OK, "Success");
        }

        [HttpPost]
        [Route("clients/changepassword")]
        [WFAuthorize(Roles = "Standard")]
        public HttpResponseMessage PostChangePassword(ChangePwdDto changePwd)
        {
            Client client = db.Clients.Find(changePwd.ID);
            if (client == null)
            {
                throw new HttpResponseException(Request.CreateResponse<string>(HttpStatusCode.NotFound,
                    string.Format("The client with the ID {0} not found.", changePwd.ID)));
            }
            else if (!PasswordHash.ValidatePassword(changePwd.OldPassword, client.Password))
            {
                throw new HttpResponseException(Request.CreateResponse<string>(HttpStatusCode.BadRequest,
                    string.Format("The old password does not match to the current password.")));
            }
            client.Password = PasswordHash.CreateHash(changePwd.NewPassword);
            SaveChanges(client);
            return Request.CreateResponse<string>(HttpStatusCode.OK, "Success");
        }

        [HttpGet]
        [Route("clients/logout")]
        [WFAuthorize(Roles = "Standard")]
        public HttpResponseMessage Logout()
        {
            string url = Request.RequestUri.Authority + Configuration.VirtualPathRoot;
            var response = Request.CreateResponse(HttpStatusCode.Redirect);
            response.Headers.Location = new Uri(Request.RequestUri.Scheme + "://" + url + "/login?action=logout");
            return response;
        }

        [HttpPost]
        [Route("purchases")]
        [WFAuthorize(Roles = "Standard")]
        public HttpResponseMessage PostPurchaseSubmit([FromBody] string items)
        {
            Client client = db.Clients.FirstOrDefault(e => e.Email.Equals(HttpContext.Current.User.Identity.Name));
            Purchase purchase = new Purchase
            {
                ClientID = client.ID,
                Date = DateTime.Now,
                Forms = items,
                Completed = true // TO DO: Remove this line once we complete the payment!
            };
            db.Purchases.Add(purchase);
            db.SaveChanges();
            return Request.CreateResponse<string>(HttpStatusCode.OK, "Success");
        }

        private void SaveChanges(Client client)
        {
            client.ModifiedBy = HttpContext.Current.User.Identity.Name;
            client.ModifiedDate = DateTime.Now;
            db.SaveChanges();
        }

        private string CreateRandomPassword(int passwordLength)
        {
            string allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ1234567890";
            char[] chars = new char[passwordLength];
            byte[] randomBytes = new byte[passwordLength];
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            rng.GetBytes(randomBytes);

            for (int i = 0; i < passwordLength; i++)
            {
                chars[i] = allowedChars[(int)randomBytes[i] % allowedChars.Length];
            }

            return new string(chars);
        }
    }
}