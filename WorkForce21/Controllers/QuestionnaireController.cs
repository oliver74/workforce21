﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Web;
using System.Web.Http;
using WorkForce21.DAL;
using WorkForce21.Models;
using WorkForce21.Utilities;
using WotkForce21.Utilities;

namespace WorkForce21.Controllers
{
    [WFAuthorize(Roles = "Standard")]
    public class QuestionnaireController : ApiController
    {
        private WFContext db = new WFContext();

        [HttpGet]
        [Route("clients/{id:int}/questionnaire")]
        public IEnumerable<Questionnaire> GetClientQuestionnaire(int id)
        {
            return db.Questionnaires.Where(e => e.ClientID == id);
        }

        [HttpPost]
        [Route("clients/{id:int}/questionnaire")]
        public HttpResponseMessage Post(int id, Questionnaire pQuestionnaire)
        {
            Questionnaire questionnaire = db.Questionnaires.Where(e => e.ClientID == id).FirstOrDefault();
            if (questionnaire == null)
            {
                // First submit
                pQuestionnaire.ClientID = id;
                pQuestionnaire.AuditBeginDate = pQuestionnaire.LastModifiedDate = DateTime.Now;
                pQuestionnaire.EELessThan15 = !(pQuestionnaire.EE50OrMore || pQuestionnaire.EE20_49 || pQuestionnaire.EE15_19);
                db.Questionnaires.Add(pQuestionnaire);
            }
            else
            {
                DateTime temp = questionnaire.AuditBeginDate;
                Reflection.CopyProperties(questionnaire, pQuestionnaire);
                questionnaire.EELessThan15 = !(pQuestionnaire.EE50OrMore || pQuestionnaire.EE20_49 || pQuestionnaire.EE15_19);
                questionnaire.ClientID = id;
                questionnaire.AuditBeginDate = temp;
                questionnaire.LastModifiedDate = DateTime.Now;
            }

            db.SaveChanges();
            return Request.CreateResponse<string>(HttpStatusCode.OK, "Success");
        }
    }
}