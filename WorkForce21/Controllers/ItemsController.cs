﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Web;
using System.Web.Http;
using WorkForce21.DAL;
using WorkForce21.Models;
using WorkForce21.Utilities;
using WotkForce21.Utilities;

namespace WorkForce21.Controllers
{
    [WFAuthorize(Roles = "Standard")]
    public class ItemsController : ApiController
    {
        private WFContext db = new WFContext();

        [HttpGet]
        [Route("i9items")]
        public IEnumerable<I9Item> GetI9Items()
        {
            return db.I9Items.Where(e => !db.I9Answers.Any(a => e.ID == a.ItemID)).ToList();
        }

        [HttpGet]
        [Route("i9itemsedit")]
        public IEnumerable<I9Item> GetI9ItemsEdit()
        {
            Client client = db.Clients.FirstOrDefault(e => e.Email.Equals(HttpContext.Current.User.Identity.Name));
            IEnumerable<I9Answer> answers = db.I9Answers.Where(e => e.ClientID == client.ID);
            List<I9Item> retVal = db.I9Items.Where(e => db.I9Answers.Any(a => e.ID == a.ItemID)).ToList();
            retVal.ForEach(e => e.Answer = answers.First(a => a.ItemID == e.ID).Answer);
            return retVal;
        }

        [HttpPost]
        [Route("i9items")]
        public HttpResponseMessage PostI9Answer(SubmitAnswerDto answerDto)
        {
            Client client = db.Clients.FirstOrDefault(e => e.Email.Equals(HttpContext.Current.User.Identity.Name));
            I9Answer answer = db.I9Answers.Find(client.ID, answerDto.ItemID);
            if (answer != null) answer.Answer = answerDto.AnswerID;
            else
            {
                answer = new I9Answer { ClientID = client.ID, ItemID = answerDto.ItemID, Answer = answerDto.AnswerID };
                db.I9Answers.Add(answer);
            }
            db.SaveChanges();
            return Request.CreateResponse(HttpStatusCode.OK, "Success");
        }

        [HttpGet]
        [Route("adaitems")]
        public IEnumerable<AdaItem> GetAdaItems()
        {
            return db.AdaItems.Where(e => !db.AdaAnswers.Any(a => e.ID == a.ItemID)).ToList();
        }

        [HttpGet]
        [Route("adaitemsedit")]
        public IEnumerable<AdaItem> GetAdaItemsEdit()
        {
            Client client = db.Clients.FirstOrDefault(e => e.Email.Equals(HttpContext.Current.User.Identity.Name));
            IEnumerable<AdaAnswer> answers = db.AdaAnswers.Where(e => e.ClientID == client.ID);
            List<AdaItem> retVal = db.AdaItems.Where(e => db.AdaAnswers.Any(a => e.ID == a.ItemID)).ToList();
            retVal.ForEach(e => e.Answer = answers.First(a => a.ItemID == e.ID).Answer);
            return retVal;
        }

        [HttpPost]
        [Route("adaitems")]
        public HttpResponseMessage PostAdaAnswer(SubmitAnswerDto answerDto)
        {
            Client client = db.Clients.FirstOrDefault(e => e.Email.Equals(HttpContext.Current.User.Identity.Name));
            AdaAnswer answer = db.AdaAnswers.Find(client.ID, answerDto.ItemID);
            if (answer != null) answer.Answer = answerDto.AnswerID;
            else
            {
                answer = new AdaAnswer { ClientID = client.ID, ItemID = answerDto.ItemID, Answer = answerDto.AnswerID };
                db.AdaAnswers.Add(answer);
            }
            db.SaveChanges();
            return Request.CreateResponse(HttpStatusCode.OK, "Success");
        }

        [HttpGet]
        [Route("flsaitems")]
        public IEnumerable<FlsaItem> GetFlsaItems()
        {
            return db.FlsaItems.Where(e => !db.FlsaAnswers.Any(a => e.ID == a.ItemID)).ToList();
        }

        [HttpGet]
        [Route("flsaitemsedit")]
        public IEnumerable<FlsaItem> GetFlsaItemsEdit()
        {
            Client client = db.Clients.FirstOrDefault(e => e.Email.Equals(HttpContext.Current.User.Identity.Name));
            IEnumerable<FlsaAnswer> answers = db.FlsaAnswers.Where(e => e.ClientID == client.ID);
            List<FlsaItem> retVal = db.FlsaItems.Where(e => db.FlsaAnswers.Any(a => e.ID == a.ItemID)).ToList();
            retVal.ForEach(e => e.Answer = answers.First(a => a.ItemID == e.ID).Answer);
            return retVal;
        }

        [HttpPost]
        [Route("flsaitems")]
        public HttpResponseMessage PostFlsaAnswer(SubmitAnswerDto answerDto)
        {
            Client client = db.Clients.FirstOrDefault(e => e.Email.Equals(HttpContext.Current.User.Identity.Name));
            FlsaAnswer answer = db.FlsaAnswers.Find(client.ID, answerDto.ItemID);
            if (answer != null) answer.Answer = answerDto.AnswerID;
            else
            {
                answer = new FlsaAnswer { ClientID = client.ID, ItemID = answerDto.ItemID, Answer = answerDto.AnswerID };
                db.FlsaAnswers.Add(answer);
            }
            db.SaveChanges();
            return Request.CreateResponse(HttpStatusCode.OK, "Success");
        }

        [HttpGet]
        [Route("fmlaitems")]
        public IEnumerable<FmlaItem> GetFmlaItems()
        {
            return db.FmlaItems.Where(e => !db.FmlaAnswers.Any(a => e.ID == a.ItemID)).ToList();
        }

        [HttpGet]
        [Route("fmlaitemsedit")]
        public IEnumerable<FmlaItem> GetFmlaItemsEdit()
        {
            Client client = db.Clients.FirstOrDefault(e => e.Email.Equals(HttpContext.Current.User.Identity.Name));
            IEnumerable<FmlaAnswer> answers = db.FmlaAnswers.Where(e => e.ClientID == client.ID);
            List<FmlaItem> retVal = db.FmlaItems.Where(e => db.FmlaAnswers.Any(a => e.ID == a.ItemID)).ToList();
            retVal.ForEach(e => e.Answer = answers.First(a => a.ItemID == e.ID).Answer);
            return retVal;
        }

        [HttpPost]
        [Route("fmlaitems")]
        public HttpResponseMessage PostFmlaAnswer(SubmitAnswerDto answerDto)
        {
            Client client = db.Clients.FirstOrDefault(e => e.Email.Equals(HttpContext.Current.User.Identity.Name));
            FmlaAnswer answer = db.FmlaAnswers.Find(client.ID, answerDto.ItemID);
            if (answer != null) answer.Answer = answerDto.AnswerID;
            else
            {
                answer = new FmlaAnswer { ClientID = client.ID, ItemID = answerDto.ItemID, Answer = answerDto.AnswerID };
                db.FmlaAnswers.Add(answer);
            }
            db.SaveChanges();
            return Request.CreateResponse(HttpStatusCode.OK, "Success");
        }

        [HttpGet]
        [Route("hpsponsoritems")]
        public IEnumerable<HPSponsorItem> GetHPSponsorItems()
        {
            return db.HPSponsorItems.Where(e => !db.HPSponsorAnswers.Any(a => e.ID == a.ItemID)).ToList();
        }

        [HttpGet]
        [Route("hpsponsoritemsedit")]
        public IEnumerable<HPSponsorItem> GetHPSponsorItemsEdit()
        {
            Client client = db.Clients.FirstOrDefault(e => e.Email.Equals(HttpContext.Current.User.Identity.Name));
            IEnumerable<HPSponsorAnswer> answers = db.HPSponsorAnswers.Where(e => e.ClientID == client.ID);
            List<HPSponsorItem> retVal = db.HPSponsorItems.Where(e => db.HPSponsorAnswers.Any(a => e.ID == a.ItemID)).ToList();
            retVal.ForEach(e => e.Answer = answers.First(a => a.ItemID == e.ID).Answer);
            return retVal;
        }

        [HttpPost]
        [Route("hpsponsoritems")]
        public HttpResponseMessage PostHPSponsorAnswer(SubmitAnswerDto answerDto)
        {
            Client client = db.Clients.FirstOrDefault(e => e.Email.Equals(HttpContext.Current.User.Identity.Name));
            HPSponsorAnswer answer = db.HPSponsorAnswers.Find(client.ID, answerDto.ItemID);
            if (answer != null) answer.Answer = answerDto.AnswerID;
            else
            {
                answer = new HPSponsorAnswer { ClientID = client.ID, ItemID = answerDto.ItemID, Answer = answerDto.AnswerID };
                db.HPSponsorAnswers.Add(answer);
            }
            db.SaveChanges();
            return Request.CreateResponse(HttpStatusCode.OK, "Success");
        }

        [HttpGet]
        [Route("contractoritems")]
        public IEnumerable<ContractorItem> GetContractorItems()
        {
            return db.ContractorItems.Where(e => !db.ContractorAnswers.Any(a => e.ID == a.ItemID)).ToList();
        }

        [HttpGet]
        [Route("contractoritemsedit")]
        public IEnumerable<ContractorItem> GetContractorItemsEdit()
        {
            Client client = db.Clients.FirstOrDefault(e => e.Email.Equals(HttpContext.Current.User.Identity.Name));
            IEnumerable<ContractorAnswer> answers = db.ContractorAnswers.Where(e => e.ClientID == client.ID);
            List<ContractorItem> retVal = db.ContractorItems.Where(e => db.ContractorAnswers.Any(a => e.ID == a.ItemID)).ToList();
            retVal.ForEach(e => e.Answer = answers.First(a => a.ItemID == e.ID).Answer);
            return retVal;
        }

        [HttpPost]
        [Route("contractoritems")]
        public HttpResponseMessage PostContractorAnswer(SubmitAnswerDto answerDto)
        {
            Client client = db.Clients.FirstOrDefault(e => e.Email.Equals(HttpContext.Current.User.Identity.Name));
            ContractorAnswer answer = db.ContractorAnswers.Find(client.ID, answerDto.ItemID);
            if (answer != null) answer.Answer = answerDto.AnswerID;
            else
            {
                answer = new ContractorAnswer { ClientID = client.ID, ItemID = answerDto.ItemID, Answer = answerDto.AnswerID };
                db.ContractorAnswers.Add(answer);
            }
            db.SaveChanges();
            return Request.CreateResponse(HttpStatusCode.OK, "Success");
        }
    }
}