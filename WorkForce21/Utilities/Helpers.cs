﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using WorkForce21.DAL;
using WorkForce21.Models;

namespace WorkForce21.Utilities
{
    public static class Helpers
    {
        public static List<string> GetPurchasedForms(int clientID)
        {
            DateTime startDate = DateTime.Now.AddDays(-30);
            List<string> purchasedForms = new List<string>();
            WFContext db = new WFContext();
            List<Purchase> purchases = db.Purchases.Where(p => p.ClientID == clientID && p.Completed && p.Date > startDate).ToList();
            purchases.ForEach(p => purchasedForms.AddRange(
                p.Forms.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)));
            return purchasedForms.Distinct().ToList();
        }

        public static List<string> GetExpiringForms(int clientID)
        {
            Dictionary<string, string> retVal = new Dictionary<string, string>();
            retVal["I9"] = string.Empty;
            retVal["ADA"] = string.Empty;
            retVal["FLSA"] = string.Empty;
            retVal["ADEA"] = string.Empty;
            retVal["FMLA"] = string.Empty;
            retVal["HPS"] = string.Empty;
            retVal["CR"] = string.Empty;

            DateTime startDate = DateTime.Now.AddDays(-30);
            List<string> purchasedForms = new List<string>();
            WFContext db = new WFContext();
            List<Purchase> purchases = db.Purchases.Where(p => p.ClientID == clientID && p.Completed && p.Date > startDate).ToList();
            foreach (Purchase purchase in purchases)
            {
                TimeSpan ts = purchase.Date.AddDays(30).Subtract(DateTime.Now);
                string message = string.Empty;
                if (ts.Days > 0) message = ts.Days.ToString() + " Days";
                else if (ts.Hours > 0) message = ts.Hours.ToString() + " Hours";
                else if (ts.Minutes > 0) message = ts.Minutes.ToString() + " Minutes";

                string[] items = purchase.Forms.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string item in items) retVal[item] = message;
            }

            return retVal.Values.ToList();
        }
    }
}