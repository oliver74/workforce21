﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using WorkForce21.DAL;
using WorkForce21.Models;

namespace WorkForce21.Utilities
{
    public class WFAuthorize : AuthorizeAttribute
    {
        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            bool retVal = false;
            WFContext db = new WFContext();
            Client client = db.Clients.FirstOrDefault(e => e.Email == HttpContext.Current.User.Identity.Name);
            if (client.Admin || Roles.Contains("Standard")) retVal = true;
            return retVal;
        }
    }
}