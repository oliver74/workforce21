﻿<%@ Page Title="Assess My Compliance" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WorkForce21.Views._Default" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <div class="container">
     <%--   <div class="row">
            <div class="col col-sm-12">

                <div class="panel">
                    <div class="panel-body">
                        You may want to put some news here  <span class="glyphicon glyphicon-heart-empty"></span>
                    </div>
                </div><a href="Default.aspx">Default.aspx</a>

            </div>
        </div>--%>
        <div class="row">
            <div class="col col-sm-3">
                <div id="sidebar">
                    <ul class="nav navbar-stacked">
                        <li>
                            <h3 class="highlight">WorkForce21 <i class="glyphicon glyphicon-info-sign pull-right"></i></h3>
                        </li>
                    </ul>
                    <div>
                        <a href="https://www.facebook.com/wforce21/timeline" target="_blank">
                            <img src="<%= Page.ResolveClientUrl("~/Images/facebook.png") %>" title="Follow Us on Facebook" style="opacity: 0.8; -moz-opacity: 0.8;" class="fade"/>
                        </a>
                        <a href="https://twitter.com/workforce21" target="_blank">
                            <img src="<%= Page.ResolveClientUrl("~/Images/twitter.png") %>" title="Follow Us on Twitter" style="opacity: 0.8; -moz-opacity: 0.8;" class="fade"/>
                        </a>
                        <a href="https://www.linkedin.com/pub/dan-farrar-dfarrar-wforce21-com/b/667/b92" target="_blank">
                            <img src="<%= Page.ResolveClientUrl("~/Images/linkedin.png") %>" title="Follow Us on LinkedIn" style="opacity: 0.8; -moz-opacity: 0.8;" class="fade"/>
                        </a>
                    </div>
                    <br />
                    <div class="accordion" id="accordion2">
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">About
                            </a>
                            </div>
                            <div id="collapseOne" class="accordion-body collapse">
                                <div class="accordion-inner">
                                    <p>WorkForce21 specializes in Human Resource and workforce management support services for small and mid-sized business. We are proud of the quality of our services and the thoroughness by which we conduct our work. We provide our clients with professional quality services through a highly trained and experienced group of people, all dedicated to improving you workforce management capability.</p>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">Contact
                            </a>
                            </div>
                            <div id="collapseTwo" class="accordion-body collapse">
                                <div class="accordion-inner">
                                    <p>
                                        (888) 237-4882
                                   
                                        <br />
                                        21@wforce21.com
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br />
                </div>
            </div>
            <div class="col col-sm-9">
                <div class="panel">
                    <div class="tab-pane">
                        <div id="rootwizard">
                            <div style="display: none;">
                                <div class="navbar-inner">
                                    <div class="container">
                                        <ul>
                                            <li><a href="#tab1" data-toggle="tab">First</a></li>
                                            <li><a href="#tab2" data-toggle="tab">Second</a></li>
                                            <li><a href="#tab3" data-toggle="tab">Third</a></li>
                                            <li><a href="#tab4" data-toggle="tab">Forth</a></li>
                                            <li><a href="#tab5" data-toggle="tab">Fifth</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane" id="tab1">
                                    <br />
                                    Does your company employ 50 or more employees for at least 20 workweeks in the current or preceding calendar year at one or more worksites within 75 miles?                                
                                    <br />
                                    <br />
                                    <button class="btn btn-info" data-btnyes="true">Yes</button>
                                    <button class="btn btn-info" data-btnno="true">No</button>
                                    <hr />
                                </div>
                                <div class="tab-pane" id="tab2">
                                    <br />
                                    Does your company employ 20 or more employees for at least 20 workweeks in the current or preceding calendar year?
                                    <br />
                                    <br />
                                    <button class="btn btn-info" data-btnyes="true">Yes</button>
                                    <button class="btn btn-info" data-btnno="true">No</button>
                                    <hr />
                                </div>
                                <div class="tab-pane" id="tab3">
                                    <br />
                                    Does your company employ 15 or more employees for at least 20 workweeks in the current or preceding calendar year?
                                    <br />
                                    <br />
                                    <button class="btn btn-info" data-btnyes="true">Yes</button>
                                    <button class="btn btn-info" data-btnno="true">No</button>
                                    <hr />
                                </div>
                                <div class="tab-pane" id="tab4">
                                    <br />
                                    Does your company provide eligible employees with an offer of health insurance that is partially or fully paid for by the company?
                                    <br />
                                    <br />
                                    <button class="btn btn-info" data-btnyes="true">Yes</button>
                                    <button class="btn btn-info" data-btnno="true">No</button>
                                    <hr />
                                </div>
                                <div class="tab-pane" id="tab5">
                                    <br />
                                    Does your company procure third parties that are paid as a 1099 contractor?
                                    <br />
                                    <br />
                                    <button class="btn btn-info" data-btnyes="true">Yes</button>
                                    <button class="btn btn-info" data-btnno="true">No</button>
                                    <hr />
                                </div>
                                <ul class="pager wizard">
                                    <li class="previous first" style="display: none;"><a href="#">First</a></li>
                                    <li class="previous"><a href="#" id="btnPrevious">Previous</a></li>
                                    <li class="next last" style="display: none;"><a href="#">Last</a></li>
                                    <li class="next" style="display: none;"><a href="#">Next</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="table-responsive" id="quesresult">
                            <br />
                            <table class="table table-condensed table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Must I Comply?</th>
                                    <th>Compliance Requirement</th>
                                    <th>Government Entity</th>
                                    <th>Agency</th>
                                    <th>More Info</th>
                                    <th>License Expiration</th>
                                    <th>Purchase</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">1</th>
                                    <td id="tdReq1" data-tdreq="true">No</td>
                                    <td>Right to work in the United States (Form I-9)</td>
                                    <td>Homeland Security</td>
                                    <td>ICE</td>
                                    <td><button class="btn btn-info" data-moreinfo="true" data-item="I9">More Info</button></td>
                                    <td id="tdExp1"></td>
                                    <td align="center"><input type="checkbox" data-purchase="true" data-item="I9" disabled/></td>
                                </tr>
                                <tr>
                                    <th scope="row">2</th>
                                    <td id="tdReq2" data-tdreq="true">No</td>
                                    <td>Americans with Disabilities Act</td>
                                    <td>Equal Employment Opportunity</td>
                                    <td>EEOC</td>
                                    <td><button class="btn btn-info" data-moreinfo="true" data-item="ADA">More Info</button></td>
                                    <td id="tdExp2"></td>
                                    <td align="center"><input type="checkbox" data-purchase="true" data-item="ADA" disabled/></td>
                                </tr>
                                <tr>
                                    <th scope="row">3</th>
                                    <td id="tdReq3" data-tdreq="true">No</td>
                                    <td>Fair Labor Standards Act</td>
                                    <td>Department of Labor</td>
                                    <td>WHD</td>
                                    <td><button class="btn btn-info" data-moreinfo="true" data-item="FLSA">More Info</button></td>
                                    <td id="tdExp3"></td>
                                    <td align="center"><input type="checkbox" data-purchase="true" data-item="FLSA" disabled/></td>
                                </tr>
                                <tr>
                                    <th scope="row">4</th>
                                    <td id="tdReq4" data-tdreq="true">No</td>
                                    <td>Age Discrimination in Employment Act</td>
                                    <td>Equal Employment Opportunity</td>
                                    <td>EEOC</td>
                                    <td><button class="btn btn-info" data-moreinfo="true" data-item="ADEA">More Info</button></td>
                                    <td id="tdExp4"></td>
                                    <td align="center"><input type="checkbox" data-purchase="true" data-item="ADEA" disabled/></td>
                                </tr>
                                <tr>
                                    <th scope="row">5</th>
                                    <td id="tdReq5" data-tdreq="true">No</td>
                                    <td>Family Medical Leave Act</td>
                                    <td>Department of Labor</td>
                                    <td>WHD</td>
                                    <td><button class="btn btn-info" data-moreinfo="true" data-item="FMLA">More Info</button></td>
                                    <td id="tdExp5"></td>
                                    <td align="center"><input type="checkbox" data-purchase="true" data-item="FMLA" disabled/></td>
                                </tr>
                                <tr>
                                    <th scope="row">6</th>
                                    <td id="tdReq6" data-tdreq="true">No</td>
                                    <td>Health Plan Sponsorship</td>
                                    <td>Health & Human Services</td>
                                    <td>OCR</td>
                                    <td><button class="btn btn-info" data-moreinfo="true" data-item="HPS">More Info</button></td>
                                    <td id="tdExp6"></td>
                                    <td align="center"><input type="checkbox" data-purchase="true" data-item="HPS" disabled/></td>
                                </tr>
                                <tr>
                                    <th scope="row">7</th>
                                    <td id="tdReq7" data-tdreq="true">No</td>
                                    <td>Contractor Relationship</td>
                                    <td>Department of Labor</td>
                                    <td>DOL</td>
                                    <td><button class="btn btn-info" data-moreinfo="true" data-item="CR">More Info</button></td>
                                    <td id="tdExp7"></td>
                                    <td align="center"><input type="checkbox" data-purchase="true" data-item="CR" disabled/></td>
                                </tr>
                            </tbody>
                            </table>
                            <br />
                            <button class="btn btn-info" id="btnResetQuest">Reset Results</button>
                            <button class="btn btn-info pull-right" id="btnPurchase">Purchase</button>
                        </div>
                    </div>
                    <h1><a href="#"><i class="glyphicon glyphicon-chevron-up"></i></a></h1>
                    <hr/>
                </div>
            </div>
        </div>
    </div>

    <div id="moreInfoDlg" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
       <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
               <h4 class="modal-title">More Info</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
              <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Close</button>
            </div>
          </div><!-- /.modal-content -->
       </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <script src="<%= Page.ResolveClientUrl("~/scripts/custom/globals.js") %>"></script>
    <script src="<%= Page.ResolveClientUrl("~/scripts/custom/default.aspx.js") %>"></script>
    <form id="frm" runat="server" />
</asp:Content>
