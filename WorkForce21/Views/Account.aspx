﻿<%@ Page Title="Account" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true" CodeBehind="Account.aspx.cs" Inherits="WorkForce21.Views.Account" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
        <div class="col col-sm-12">
           <div class="form-group">
              <label for="txtLoginName">Login Name</label>
              <input type="text" class="form-control" id="txtLoginName" disabled />
           </div>
           <div class="form-group">
              <a id="aUpdatePassword" href="#">Change Password</a>
           </div>
           <div class="form-group">
              <label for="txtFirstName">First Name</label>
              <input type="text" class="form-control" id="txtFirstName" />
           </div>
           <div class="form-group">
              <label for="txtLastName">Last Name</label>
              <input type="text" class="form-control" id="txtLastName" />
           </div>
           <button id="cmdUpdateProfile" type="submit" class="btn btn-primary">Update Profile</button>
        </div>
    </div>

    <div id="changePassword" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
               <h4 class="modal-title">Change Password</h4>
            </div>
            <div class="modal-body">
                <p>
                    <label>Old Password</label>
                    <input type="password" id="txtOldPassword" class="form-control account-inputs" placeholder="Old password..." />
                </p>
                <br />
                <p>
                    <label>New Password</label>
                    <input type="password" id="txtNewPassword" class="form-control account-inputs" placeholder="New password..." />
                </p>
                <br />
                <p>
                    <label>Confirm Password</label>
                    <input type="password" id="txtConfirmPassword" class="form-control account-inputs" placeholder="Confirm password..." />
                </p>
           </div>
           <div class="modal-footer">
              <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
              <button id="cmdChangePassword" class="btn btn-primary">Save changes</button>
           </div>
         </div><!-- /.modal-content -->
       </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <script src="<%= Page.ResolveClientUrl("~/scripts/custom/globals.js") %>"></script>
    <script src="<%= Page.ResolveClientUrl("~/scripts/custom/account.aspx.js") %>"></script>
    <form id="frm" runat="server" />
</asp:Content>
