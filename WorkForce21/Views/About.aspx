﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="WorkForce21.Views.About" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <div class="container">
        <div class="col col-sm-12">
            <div class="panel">
                <hgroup class="title">
                    <h1>About WorkForce21.</h1>
                </hgroup>
                <article>
                    <p style="text-align: justify;"><strong>WorkForce<span style="color: #c29d34;">21</span></strong> specializes in Human Resource and workforce management support services for small and mid-sized business. We are proud of the quality of our services and the thoroughness by which we conduct our work.&nbsp;We provide our clients with professional quality services through a highly trained and experienced group of people, all dedicated to improving you workforce management capability.</p>
                    <p><em>Our core services include:</em></p>
                    <p><span style="text-decoration: underline; color: #333333;"><strong><a title="Audits" href="http://wforce21.com/human-resources/audits/" target="_blank">Human Resource Audits:</a></strong></span> We will conduct a full operational audit or selective components that you want us to audit such as Form I-9, Fair Labor Standards Act compliance, job descriptions or other area that most concern you.</p>
                    <p><span style="text-decoration: underline; color: #333333;"><strong><a title="Downsizing/Outplacement" href="http://wforce21.com/human-resources/downsizingoutplacement/" target="_blank">Outplacement:</a></strong></span> Our outplacement services are personalized and include significant contact between the affected employee and their&nbsp;<strong>WorkForce<span style="color: #e0b300;">21</span></strong> career counselor.&nbsp; Services are designed to help the former employee(s) become reemployed through a strategic job search plan, resume development, and interviewing skills.</p>
                    <p><span style="text-decoration: underline; color: #333333;"><strong><a title="TRAINING" href="http://wforce21.com/training/" target="_blank">Training</a>:</strong></span> Our employee training services include courses for management as well as employees. Our classes can be delivered on-site or via webinar format.</p>
                    <p><span style="text-decoration: underline; color: #333333;"><strong><a title="Human Resources" href="http://wforce21.com/training/human-resources/" target="_blank">Handbooks</a>:</strong></span> Employee handbooks form the foundation of your Human Resource strategy, the expectations of your workforce, and management team. Our handbooks are not simply a template to provide a generic ‘feel good’ solution. We develop these handbooks to support your decision making processes related to employee treatment.</p>
                    <p><span style="text-decoration: underline; color: #333333;"><strong><a title="ERISA" href="http://wforce21.com/erisa/" target="_blank">ERISA</a>:</strong></span> We provide you with ERISA compliant Summary Plan Descriptions that are required to be distributed to plan participants within specific time periods. We will also prepare your Form 5500’s for filing with the Department of Labor.</p>
                    <p><span style="text-decoration: underline; color: #333333;"><strong><a title="ACA–HEALTHCARE LAW" href="http://wforce21.com/aca-healthcare-law/" target="_blank">New Healthcare Legislation/ACA</a>:</strong></span> <strong>WorkForce<span style="color: #c29d34;">21</span></strong> has worked hard to become an expert in the new healthcare legislation and its likely impact on business. We provide consultation services and training related to this new landmark legislation.</p>
                </article>
            </div>
        </div>
    </div>
</asp:Content>
