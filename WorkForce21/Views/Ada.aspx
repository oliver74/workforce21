﻿<%@ Page Title="ADA" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Ada.aspx.cs" Inherits="WorkForce21.Views.Ada" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <div class="container">
        <div class="row">
            <div class="col col-sm-3">
                <div id="sidebar">
                    <ul class="nav navbar-stacked">
                        <li>
                            <h3 class="highlight">WorkForce21 <i class="glyphicon glyphicon-info-sign pull-right"></i></h3>
                        </li>
                    </ul>
                    <div>
                        <a href="https://www.facebook.com/wforce21/timeline" target="_blank">
                            <img src="<%= Page.ResolveClientUrl("~/Images/facebook.png") %>" title="Follow Us on Facebook" style="opacity: 0.8; -moz-opacity: 0.8;" class="fade" />
                        </a>
                        <a href="https://twitter.com/workforce21" target="_blank">
                            <img src="<%= Page.ResolveClientUrl("~/Images/twitter.png") %>" title="Follow Us on Twitter" style="opacity: 0.8; -moz-opacity: 0.8;" class="fade" />
                        </a>
                        <a href="https://www.linkedin.com/pub/dan-farrar-dfarrar-wforce21-com/b/667/b92" target="_blank">
                            <img src="<%= Page.ResolveClientUrl("~/Images/linkedin.png") %>" title="Follow Us on LinkedIn" style="opacity: 0.8; -moz-opacity: 0.8;" class="fade" />
                        </a>
                    </div>
                    <br />
                    <div class="accordion" id="accordion2">
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">About
                                </a>
                            </div>
                            <div id="collapseOne" class="accordion-body collapse">
                                <div class="accordion-inner">
                                    <p>WorkForce21 specializes in Human Resource and workforce management support services for small and mid-sized business. We are proud of the quality of our services and the thoroughness by which we conduct our work. We provide our clients with professional quality services through a highly trained and experienced group of people, all dedicated to improving you workforce management capability.</p>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">Contact
                                </a>
                            </div>
                            <div id="collapseTwo" class="accordion-body collapse">
                                <div class="accordion-inner">
                                    <p>
                                        (888) 237-4882
                                   
                                        <br />
                                        21@wforce21.com
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br />
                </div>
            </div>
            <div class="col col-sm-9">
                <div class="panel">
                    <div role="tabpanel" class="tab-pane" id="sections">
                        <div class="accordion" id="panelaccordion">
                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#panelaccordion" href="#collapsePanelOne">
                                        <h2>ADA</h2>
                                    </a>
                                </div>
                                <div id="collapsePanelOne" class="accordion-body collapse">
                                    <div class="accordion-inner" id="collapsePanelOneInner">
                                    </div>
                                </div>
                            </div>
                            <h1><a href="#"><i class="glyphicon glyphicon-chevron-up"></i></a></h1>
                            <hr />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="<%= Page.ResolveClientUrl("~/scripts/custom/globals.js") %>"></script>
    <script src="<%= Page.ResolveClientUrl("~/Scripts/Custom/ada.aspx.js") %>"></script>
    <form id="frm" runat="server" />
</asp:Content>
