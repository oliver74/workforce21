﻿<%@ Page Title="Administration" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Administration.aspx.cs" Inherits="WorkForce21.Views.Administration" %>

<asp:Content ID="AdministrationPage" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <button class="btn btn-info" data-addclient="true">Add Client</button>
                <div class="table-responsive">
                    <br />
                    <table class="table table-condensed table-striped table-bordered" id="tblClients">
                        <thead>
                            <tr>
                                <th>Action</th>
                                <th>Company</th>
                                <th>Address</th>
                                <th>City</th>
                                <th>Zip</th>
                                <th>Contact Person</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>Admin</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div id="confirmActionDlg" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Confirmation</h4>
                </div>
                <div class="modal-body">
                    <p>
                        <input type="hidden" id="hfClientAction" />
                        <input type="hidden" id="hfClientId" />
                        <span class="field">
                            <label id="lblActionMessage"></label>
                        </span>
                    </p>
                </div>
                <div class="modal-footer">
                    <button id="cmdActionYes" class="btn btn-primary">Yes</button>
                    <button class="btn" data-dismiss="modal" aria-hidden="true">No</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <div id="addClientDlg" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add Client</h4>
                </div>
                <div class="modal-body" id="addAdvisorDlgBody">
                    <div class="form-group">
                        <label for="txtCompany">Company</label>
                        <input type="text" class="form-control" id="txtCompany" placeholder="Company Name..." data-addmandatory="true" />
                    </div>
                    <br />
                    <div class="form-group">
                        <label for="txtAddress">Address</label>
                        <input type="text" class="form-control" id="txtAddress" placeholder="HQ Address..." />
                    </div>
                    <br />
                    <div class="form-group">
                        <label for="txtCity">City</label>
                        <input type="text" class="form-control" id="txtCity" placeholder="HQ City..." />
                    </div>
                    <br />
                    <div class="form-group">
                        <label for="txtZip">Zip</label>
                        <input type="text" class="form-control" id="txtZip" placeholder="HQ Zip..." />
                    </div>
                    <br />
                    <div class="form-group">
                        <label for="txtFirstName">First Name</label>
                        <input type="text" class="form-control" id="txtFirstName" placeholder="HQ Contact Person First Name..." data-addmandatory="true" />
                    </div>
                    <br />
                    <div class="form-group">
                        <label for="txtLastName">Last Name</label>
                        <input type="text" class="form-control" id="txtLastName" placeholder="HQ Contact Person Last Name..." data-addmandatory="true" />
                    </div>
                    <br />
                    <div class="form-group">
                        <label for="txtPhone">Phone</label>
                        <input type="text" class="form-control" id="txtPhone" placeholder="HQ Contact Person Phone..." />
                    </div>
                    <br />
                    <div class="form-group">
                        <label for="txtEmail">Email</label>
                        <input type="text" class="form-control" id="txtEmail" placeholder="HQ Contact Person Email..." data-addmandatory="true" />
                    </div>
                    <br />
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" id="cbIsAdmin" />
                            Admin?
                        </label>
                    </div>
                    <br />
                </div>
                <div class="modal-footer">
                    <button id="cmdSaveClient" class="btn btn-primary">Save</button>
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <script src="<%= Page.ResolveClientUrl("~/scripts/custom/globals.js") %>"></script>
    <script src="<%= Page.ResolveClientUrl("~/scripts/custom/administration.aspx.js") %>"></script>
    <form id="frm" runat="server" />
</asp:Content>
