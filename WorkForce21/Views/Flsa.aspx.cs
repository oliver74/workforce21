﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WorkForce21.DAL;
using WorkForce21.Models;
using WorkForce21.Utilities;

namespace WorkForce21.Views
{
    public partial class Flsa : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Client client = Session["Client"] as Client;
            if (!client.Admin && !Helpers.GetPurchasedForms(client.ID).Contains("FLSA")) Response.Redirect("~/Questionnaire");

            WFContext db = new WFContext();
            Page.ClientScript.RegisterArrayDeclaration("buttonOptions", JsonConvert.SerializeObject(db.ButtonOptions));
        }
    }
}