﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WorkForce21.Models;
using WorkForce21.Utilities;

namespace WorkForce21.Views
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Client client = Session["Client"] as Client;
            if (client != null)
            {
                Page.ClientScript.RegisterArrayDeclaration("clientid", JsonConvert.SerializeObject(client.ID.ToString()));
                List<string> purchaseditems = Helpers.GetPurchasedForms(client.ID);
                Page.ClientScript.RegisterArrayDeclaration("purchaseditems", JsonConvert.SerializeObject(purchaseditems));

                List<string> expiringitems = Helpers.GetExpiringForms(client.ID);
                Page.ClientScript.RegisterArrayDeclaration("expiringitems", JsonConvert.SerializeObject(expiringitems));
            }
            if (Request.RawUrl.Contains("Default.aspx") || Request.RawUrl == Request.ApplicationPath) Response.Redirect("~/Questionnaire");
        }
    }
}