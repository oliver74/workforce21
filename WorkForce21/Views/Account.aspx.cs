﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using Newtonsoft.Json;
using WorkForce21.Models;
using WorkForce21.DAL;

namespace WorkForce21.Views
{
    public partial class Account : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Client client = (Client)Session["Client"];
            WFContext db = new WFContext();
            client = db.Clients.Find(client.ID);
            Session["Client"] = client;
            Page.ClientScript.RegisterArrayDeclaration("accountData", JsonConvert.SerializeObject(client));
        }
    }
}